﻿using AutoMapper;
using PhotoContest.Data.Models;
using PhotoContest.Services.DTOs;

namespace PhotoContest.Services.Mapping
{
    public class DTOMapper : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DTOMapper"/> class.
        /// </summary>
        public DTOMapper()
        {
            CreateMap<Category, CategoryDTO>().ReverseMap();
            CreateMap<Review, ReviewDTO>().ReverseMap();
            CreateMap<Image, ImageDTO>().ReverseMap();
            CreateMap<Photo, PhotoDTO>().ReverseMap();
            CreateMap<Contest, ContestDTO>().ReverseMap();
            CreateMap<User, UserDTO>().ForMember(x => x.AvatarPath, z => z.MapFrom(y => y.Avatar.ImageUrl));
            CreateMap<UserDTO, User>();
            CreateMap<Notification, NotificationDTO>();
        }
    }
}
