﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Services.Helpers.Attributes
{

    public class PhaseOneFinishTimeAttribute : ValidationAttribute
    {

        public PhaseOneFinishTimeAttribute() { }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <returns>Error message</returns>
        public static string GetErrorMessage() => "Date must be 6+ hours later than now.";


        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime date = (DateTime)value;

            if (DateTime.Compare(date, DateTime.Now.AddDays(1)) < 0
                && DateTime.Compare(date, DateTime.Now.AddMonths(1)) < 0)
            {
                return new ValidationResult(GetErrorMessage());
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }

}
