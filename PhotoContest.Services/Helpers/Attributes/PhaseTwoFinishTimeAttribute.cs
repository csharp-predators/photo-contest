﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Services.Helpers.Attributes
{

    public class PhaseTwoFinishTimeAttribute : ValidationAttribute
    {

        private readonly string comparisonProperty;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhaseTwoFinishTimeAttribute"/> class.
        /// </summary>
        /// <param name="comparisonProperty">The comparison property.</param>
        public PhaseTwoFinishTimeAttribute(string comparisonProperty)
        {
            this.comparisonProperty = comparisonProperty;
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <returns>Error message</returns>
        public static string GetErrorMessage() => "Date must be 6+ hours later than now.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime date = (DateTime)value;

            System.Reflection.PropertyInfo property = validationContext.ObjectType.GetProperty(comparisonProperty);
            DateTime comparisonValue = (DateTime)property.GetValue(validationContext.ObjectInstance);

            if (DateTime.Compare(date, comparisonValue.AddHours(1)) <= 0 && DateTime.Compare(date, comparisonValue.AddDays(1)) <= 0)
            {
                return new ValidationResult(GetErrorMessage());
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }

}
