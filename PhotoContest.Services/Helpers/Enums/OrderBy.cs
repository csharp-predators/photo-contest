﻿namespace PhotoContest.Services.Helpers.Enums
{
    public enum OrderBy
    {
        Asc,
        Desc
    }
}
