﻿namespace PhotoContest.Services.Helpers.Enums
{
    public enum SortBy
    {
        Title,
        Category,
        Creator,
        Phase,
        RemainingTime
    }
}
