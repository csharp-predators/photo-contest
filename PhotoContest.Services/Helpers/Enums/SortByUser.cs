﻿namespace PhotoContest.Services.Helpers.Enums
{
    public enum SortByUser
    {
        UserName,
        FirstName,
        LastName,
        Points,
        Rank
    }
}
