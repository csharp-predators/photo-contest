﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using PhotoContest.Services.Contracts;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class CloudinaryService : ICloudinaryService
    {
        #region Configuration

        /// <summary>
        /// The cloud account
        /// </summary>
        private static readonly Account cloudAccount = new Account { ApiKey = "965729295648465", ApiSecret = "S0iBCYZzhzPjdZPSGUtWARTDLMU", Cloud = "predatorsphotocloud" };
        /// <summary>
        /// The cloudinary
        /// </summary>
        private readonly Cloudinary cloudinary = new Cloudinary(cloudAccount);

        #endregion

        /// <summary>
        /// Uploads the photo asynchronous.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>ImageUploadResult</returns>
        public async Task<ImageUploadResult> UploadPhotoAsync(IFormFile file)
        {
            ImageUploadParams uploadParams = new ImageUploadParams()
            {
                Folder = "photos",
                File = new FileDescription(file.FileName, file.OpenReadStream()),
            };

            ImageUploadResult uploadResult = await cloudinary.UploadAsync(uploadParams);

            return uploadResult;
        }

        /// <summary>
        /// Uploads the avatar asynchronous.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>ImageUploadResult</returns>
        public async Task<ImageUploadResult> UploadAvatarAsync(IFormFile file)
        {
            ImageUploadParams uploadParams = new ImageUploadParams()
            {
                Folder = "avatars",
                File = new FileDescription(file.FileName, file.OpenReadStream()),
            };

            ImageUploadResult uploadResult = await cloudinary.UploadAsync(uploadParams);

            return uploadResult;
        }

        /// <summary>
        /// Uploads the cover asynchronous.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>ImageUploadResult</returns>
        public async Task<ImageUploadResult> UploadCoverAsync(IFormFile file)
        {
            ImageUploadParams uploadParams = new ImageUploadParams()
            {
                Folder = "covers",
                File = new FileDescription(file.FileName, file.OpenReadStream()),
            };

            ImageUploadResult uploadResult = await cloudinary.UploadAsync(uploadParams);

            return uploadResult;
        }

        /// <summary>
        /// Uploads the cover asynchronous.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns>ImageUploadResult</returns>
        public async Task<ImageUploadResult> UploadCoverAsync(string url)
        {
            ImageUploadParams uploadParams = new ImageUploadParams()
            {
                Folder = "covers",
                File = new FileDescription(url)
        };

            ImageUploadResult uploadResult = await cloudinary.UploadAsync(uploadParams);

            return uploadResult;
        }
    }
}
