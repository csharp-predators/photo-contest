﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{

    public class ValidatorService : IValidatorService
    {

        private readonly PhotoContestDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidatorService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ValidatorService(PhotoContestDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Checks if already uploaded photo asynchronous.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <param name="participantId">The participant identifier.</param>
        /// <returns>Bool result</returns>
        public async Task<bool> CheckAlreadyUploadedPhotoAsync(string contestTitle, Guid participantId)
        {
            Contest contest = await context.Contests
                .Include(c => c.Photos)
                    .ThenInclude(x => x.Photo)
                .FirstOrDefaultAsync(c => c.Title.ToLower() == contestTitle);

            if (contest.Photos.Any(cp => cp.Photo.AuthorId == participantId))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if already reviewed photo asynchronous.
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        /// <param name="juryId">The jury identifier.</param>
        /// <returns>Bool result</returns>
        public bool CheckAlreadyReviewedPhotoAsync(Guid photoId, Guid juryId)
        {
            return context.Reviews.Any(r => r.PhotoId == photoId && r.AuthorId == juryId);
        }

        /// <summary>
        /// Checks the jury asynchronous.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Bool result</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<bool> CheckJuryAsync(string contestTitle, Guid userId)
        {
            Contest contest = await context.Contests
            .Include(c => c.Jury)
            .FirstOrDefaultAsync(c => c.Title.ToLower() == contestTitle);

            if (contest == null)
            {
                throw new ArgumentException(Constants.Exception.INVALID_CONTEST_TITLE);
            }

            if (contest.Jury.Any(j => j.JuryId == userId && contest.Id == contest.Id))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks the admin asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Bool result</returns>
        public async Task<bool> CheckAdminAsync(Guid userId)
        {
            User user = await context.Users
            .FirstOrDefaultAsync(u => u.Id == userId);

            return user.IsAdmin;
        }

        /// <summary>
        /// Checks the participant asynchronous.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Bool result</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<bool> CheckParticipantAsync(string contestTitle, Guid userId)
        {
            Contest contest = await context.Contests
                .Include(c => c.Participants)
                .FirstOrDefaultAsync(c => c.Title.ToLower() == contestTitle);

            if (contest == null)
            {
                throw new ArgumentException(Constants.Exception.INVALID_CONTEST_TITLE);
            }

            if (contest.Participants.Any(cu => cu.ParticipantId == userId && contest.Id == contest.Id))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks the photo author asynchronous.
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Bool result</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<bool> CheckPhotoAuthorAsync(Guid photoId, Guid userId)
        {
            Photo photo = await context.Photos
                .FirstOrDefaultAsync(p => p.Id == photoId);

            if (photo == null)
            {
                throw new ArgumentException(Constants.Exception.INVALID_PHOTO_ID);
            }

            return photo.AuthorId == userId;
        }

        /// <summary>
        /// Valids the unique identifier string.
        /// </summary>
        /// <param name="guidString">The unique identifier string.</param>
        /// <returns>Bool result</returns>
        public bool ValidGuidString(string guidString)
        {
            return Guid.TryParse(guidString, out _);
        }
    }
}
