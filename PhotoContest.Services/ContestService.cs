﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class ContestService : IContestService
    {
        #region Fields and Constructor

        private readonly PhotoContestDbContext context;

        private readonly IMapper mapper;

        private readonly ICloudinaryService cloudinaryService;

        private readonly IValidatorService validatorService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContestService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="cloudinaryService">The cloudinary service.</param>
        /// <param name="validatorService">The validator service.</param>
        public ContestService(PhotoContestDbContext context,
            IMapper mapper,
            ICloudinaryService cloudinaryService,
            IValidatorService validatorService)
        {
            this.context = context;
            this.mapper = mapper;
            this.cloudinaryService = cloudinaryService;
            this.validatorService = validatorService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds the participant asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="contestTitle">The contest title.</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public async Task AddParticipantAsync(Guid userId, string contestTitle)
        {
            User participant = await context.Users
                .Include(u => u.ContestsAsParticipant)
                .FirstOrDefaultAsync(u => u.Id == userId);

            Contest contest = await context.Contests
                .Include(c => c.Participants)
                .FirstOrDefaultAsync(c => c.Title.ToLower() == contestTitle && c.Phase == Constants.Contest.PHASE_ONE_STRING);

            if (contest == null)
                throw new ArgumentException(Constants.Exception.INVALID_CONTEST_PARTICIPATING);

            if (await validatorService.CheckParticipantAsync(contestTitle, userId))
                throw new InvalidOperationException(Constants.Exception.ALREADY_JOINED);

            participant.ContestsAsParticipant
            .Add(new ContestParticipant() { ContestId = contest.Id, ParticipantId = participant.Id });

            participant.Points += 1;

            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Awards the winners points.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        public void AwardWinnersPoints(string contestTitle)
        {
            Contest contest = context.Contests
                .Include(c => c.Photos).ThenInclude(p => p.Photo).ThenInclude(x => x.Author)
                .Include(c => c.Photos).ThenInclude(p => p.Photo).ThenInclude(x => x.Reviews)
                .FirstOrDefault(c => c.Title == contestTitle);

            List<ContestPhoto> photos = contest.Photos
                .OrderByDescending(p => p.Photo.Reviews.Sum(r => r.Score))
                .ToList();

            List<ContestPhoto> firstPlaced = new List<ContestPhoto>() { photos.FirstOrDefault() };
            List<ContestPhoto> secondPlaced = new List<ContestPhoto>();
            List<ContestPhoto> thirdPlaced = new List<ContestPhoto>();

            int firstPlacedScore = firstPlaced[0].Photo.Reviews.Sum(p => p.Score);
            int secondPlacedScore = 0;
            int thirdPlacedScore = 0;

            for (int i = 1; i < photos.Count; i++)
            {
                ContestPhoto currentPhoto = photos[i];
                int currentPhotoScore = currentPhoto.Photo.Reviews.Sum(p => p.Score);

                if (firstPlacedScore == currentPhotoScore)
                {
                    firstPlaced.Add(currentPhoto);
                }
                else if (secondPlacedScore <= currentPhotoScore)
                {
                    secondPlaced.Add(currentPhoto);
                    secondPlacedScore = currentPhotoScore;
                }
                else if (thirdPlacedScore <= currentPhotoScore)
                {
                    thirdPlaced.Add(currentPhoto);
                    thirdPlacedScore = currentPhotoScore;
                }
            }

            if (firstPlaced.Count == 1)
            {
                if (firstPlacedScore / 2 < secondPlacedScore)
                {
                    firstPlaced.FirstOrDefault().Photo.Author.Points += 50;
                }
                else
                {
                    firstPlaced.FirstOrDefault().Photo.Author.Points += 75;
                }
            }
            else
            {
                foreach (ContestPhoto photo in firstPlaced)
                {
                    if (firstPlacedScore / 2 < secondPlacedScore)
                    {
                        firstPlaced.FirstOrDefault().Photo.Author.Points += 40;
                    }
                    else
                    {
                        firstPlaced.FirstOrDefault().Photo.Author.Points += 75;
                    }
                }
            }

            if (secondPlaced.Count == 1)
            {
                secondPlaced.FirstOrDefault().Photo.Author.Points += 35;
            }
            else
            {
                foreach (ContestPhoto photo in secondPlaced)
                {
                    photo.Photo.Author.Points += 25;
                }
            }

            if (thirdPlaced.Count == 1)
            {
                thirdPlaced.FirstOrDefault().Photo.Author.Points += 20;
            }
            else
            {
                foreach (ContestPhoto photo in thirdPlaced)
                {
                    photo.Photo.Author.Points += 10;
                }
            }
        }

        /// <summary>
        /// Creates the contest asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <exception cref="ArgumentException"></exception>
        public async Task CreateAsync(ContestDTO model)
        {
            if (context.Contests.Any(c => c.Title == model.Title))
                throw new ArgumentException(Constants.Exception.EXISTING_CONTEST_TITLE);

            Contest contest = mapper.Map<Contest>(model);

            if (model.IsInvitational)
                await AddInvitedParticipants(model, contest);

            await AddParticipantsInJury(model, contest);
            await AddAdminsInJury(contest);
            await SetCover(model, contest);

            context.Contests.Add(contest);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets all contests asynchronous.
        /// </summary>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        /// <returns>Collection of ContestDTO</returns>
        public async Task<ICollection<ContestDTO>> GetAllAsync(string searchFilter, SortBy? sortBy, OrderBy? orderBy)
        {
            IQueryable<Contest> query = context
                .Contests
                .Include(c => c.Category)
                .Include(c => c.Creator)
                .Include(c => c.Cover)
                .AsQueryable();

            List<Contest> contests = await SearchFilter(searchFilter, query).ToListAsync();
            contests = SortContests(contests, sortBy, orderBy).ToList();

            ICollection<ContestDTO> filteredContestsDTOs = mapper.Map<ICollection<ContestDTO>>(contests);

            return filteredContestsDTOs;
        }

        /// <summary>
        /// Gets the contest asynchronous.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <returns>ContestDTO</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<ContestDTO> GetAsync(string contestTitle)
        {
            Contest contest = await context.Contests
                .Include(c => c.Cover)
                .Include(c => c.Category)
                .Include(c => c.Creator)
                .FirstOrDefaultAsync(c => c.Title == contestTitle);

            if (contest == null)
                throw new ArgumentException(Constants.Exception.INVALID_CONTEST_TITLE);

            ContestDTO contestDTO = mapper.Map<ContestDTO>(contest);

            return contestDTO;
        }

        /// <summary>
        /// Gets contests by phase asynchronous.
        /// </summary>
        /// <param name="phase">The phase.</param>
        /// <returns>Collection of ContestDTO</returns>
        public async Task<ICollection<ContestDTO>> GetByPhaseAsync(string phase)
        {
            ICollection<Contest> contests = await GetContestsByPhaseIncludeCreator(phase);
            ICollection<ContestDTO> contestsDTOs = mapper.Map<ICollection<ContestDTO>>(contests);

            return contestsDTOs;
        }

        /// <summary>
        /// Gets contests by phase asynchronous.
        /// </summary>
        /// <param name="phase">The phase.</param>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        /// <returns>Collection of ContestDTO</returns>
        public async Task<ICollection<ContestDTO>> GetByPhaseAsync(string phase, string searchFilter, SortBy? sortBy, OrderBy? orderBy)
        {
            phase = phase == "one" ? Constants.Contest.PHASE_ONE_STRING : phase;
            phase = phase == "two" ? Constants.Contest.PHASE_TWO_STRING : phase;
            phase = phase == "finished" ? Constants.Contest.PHASE_FINISHED_STRING : phase;

            IQueryable<Contest> query = context
                .Contests
                .Include(c => c.Category)
                .Include(c => c.Creator)
                .Include(c => c.Cover)
                .Where(c => c.Phase == phase)
                .AsQueryable();

            List<Contest> contests = await SearchFilter(searchFilter, query).ToListAsync();
            contests = SortContests(contests, sortBy, orderBy).ToList();
            ICollection<ContestDTO> filteredContestsDTOs = mapper.Map<ICollection<ContestDTO>>(contests);

            return filteredContestsDTOs;
        }

        /// <summary>
        /// Gets contests by participant and status asynchronous.
        /// </summary>
        /// <param name="participantId">The participant identifier.</param>
        /// <param name="status">The status.</param>
        /// <param name="search">The search.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        /// <returns>Collection of ContestDTO</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<ICollection<ContestDTO>> GetByParticipantAndStatusAsync(Guid participantId, string status, string search, SortBy? sortBy, OrderBy? orderBy)
        {
            IQueryable<Contest> query;

            if (status == "current")
            {
                query = context.Contests
                    .Include(c => c.Category)
                    .Include(c => c.Creator)
                    .Include(c => c.Cover)
                    .Where(c => c.Participants.Any(x => x.ParticipantId == participantId))
                    .Where(c => c.Phase == Constants.Contest.PHASE_ONE_STRING || c.Phase == Constants.Contest.PHASE_TWO_STRING);
            }
            else if (status == "finished")
            {
                query = context.Contests
                    .Include(c => c.Category)
                    .Include(c => c.Creator)
                    .Include(c => c.Cover)
                    .Where(c => c.Participants.Any(x => x.ParticipantId == participantId))
                    .Where(c => c.Phase == Constants.Contest.PHASE_FINISHED_STRING);
            }
            else
            {
                throw new ArgumentException(Constants.Exception.INVALID_CONTEST_STATUS);
            }

            List<Contest> contests = await SearchFilter(search, query).ToListAsync();
            contests = SortContests(contests, sortBy, orderBy).ToList();

            ICollection<ContestDTO> contestsDTOs = mapper.Map<ICollection<ContestDTO>>(contests);

            return contestsDTOs;
        }

        /// <summary>
        /// Gets contests by participant and status asynchronous.
        /// </summary>
        /// <param name="participantId">The participant identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns>Collection of ContestDTO</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<ICollection<ContestDTO>> GetByParticipantAndStatusAsync(Guid participantId, string status)
        {
            List<Contest> contests = new List<Contest>();

            if (status == "current")
            {
                contests = await context.Contests
                    .Where(c => c.Participants.Any(x => x.ParticipantId == participantId))
                    .Where(c => c.Phase == Constants.Contest.PHASE_ONE_STRING || c.Phase == Constants.Contest.PHASE_TWO_STRING)
                    .ToListAsync();
            }
            else if (status == "finished")
            {
                contests = await context.Contests
                    .Where(c => c.Participants.Any(x => x.ParticipantId == participantId))
                    .Where(c => c.Phase == Constants.Contest.PHASE_FINISHED_STRING)
                    .ToListAsync();
            }
            else
            {
                throw new ArgumentException(Constants.Exception.INVALID_CONTEST_STATUS);
            }

            ICollection<ContestDTO> contestsDTOs = mapper.Map<ICollection<ContestDTO>>(contests);

            return contestsDTOs;
        }

        /// <summary>
        /// Gets contest by photo asynchronous.
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        /// <returns>ContestDTO</returns>
        public async Task<ContestDTO> GetByPhotoAsync(Guid photoId)
        {
            var contest = await context.Contests
                .Include(c => c.Photos)
                .FirstOrDefaultAsync(c => c.Photos.Any(p => p.PhotoId == photoId));

            var contestDTO = mapper.Map<ContestDTO>(contest);

            return contestDTO;
        }

        /// <summary>
        /// Gets contests by jury asynchronous.
        /// </summary>
        /// <param name="juryId">The jury identifier.</param>
        /// <param name="search">The search.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        /// <returns>Collection of ContestDTO</returns>
        public async Task<ICollection<ContestDTO>> GetByJuryAsync(Guid juryId, string search, SortBy? sortBy, OrderBy? orderBy)
        {
            IQueryable<Contest> query;

            query = context.Contests
                .Include(c => c.Category)
                .Include(c => c.Creator)
                .Include(c => c.Cover)
                .Where(c => c.Jury.Any(x => x.JuryId == juryId));

            List<Contest> contests = await SearchFilter(search, query).ToListAsync();
            contests = SortContests(contests, sortBy, orderBy).ToList();

            ICollection<ContestDTO> contestsDTOs = mapper.Map<ICollection<ContestDTO>>(contests);

            return contestsDTOs;
        }

        /// <summary>
        /// Gets the winners.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <returns>StandingList</returns>
        public async Task<StandingsList> GetWinners(string contestTitle)
        {
            Contest contest = await context.Contests
                .Include(c => c.Photos).ThenInclude(p => p.Photo).ThenInclude(x => x.Author)
                .Include(c => c.Photos).ThenInclude(p => p.Photo).ThenInclude(x => x.Reviews)
                .FirstOrDefaultAsync(c => c.Title == contestTitle);

            List<ContestPhoto> photos = contest.Photos
                .OrderByDescending(p => p.Photo.Reviews.Sum(r => r.Score))
                .ToList();

            List<ContestPhoto> firstPlaced = new List<ContestPhoto>() { photos.FirstOrDefault() };
            List<ContestPhoto> secondPlaced = new List<ContestPhoto>();
            List<ContestPhoto> thirdPlaced = new List<ContestPhoto>();

            int firstPlacedScore = firstPlaced[0].Photo.Reviews.Sum(p => p.Score);
            int secondPlacedScore = 0;
            int thirdPlacedScore = 0;

            for (int i = 1; i < photos.Count; i++)
            {
                ContestPhoto currentPhoto = photos[i];
                int currentPhotoScore = currentPhoto.Photo.Reviews.Sum(p => p.Score);

                if (firstPlacedScore == currentPhotoScore)
                {
                    firstPlaced.Add(currentPhoto);
                }
                else if (secondPlacedScore <= currentPhotoScore)
                {
                    secondPlaced.Add(currentPhoto);
                    secondPlacedScore = currentPhotoScore;
                }
                else if (thirdPlacedScore <= currentPhotoScore)
                {
                    thirdPlaced.Add(currentPhoto);
                    thirdPlacedScore = currentPhotoScore;
                }
                else
                {
                    break;
                }
            }

            List<ContestPhoto> lowPlaced = photos
                .Skip(firstPlaced.Count + secondPlaced.Count + thirdPlaced.Count)
                .ToList();

            var standingList = new StandingsList 
            { 
                FirstPlaced = firstPlaced, 
                SecondPaced = secondPlaced, 
                ThirdPlaced = thirdPlaced,
                LowPlaced = lowPlaced
            };

            return standingList;
        }

        /// <summary>
        /// Notificates participants for phase finishes.
        /// </summary>
        /// <param name="contest">The contest.</param>
        public void NotificateFinish(Contest contest)
        {
            // notificate participants for finish
            foreach (var item in contest.Participants)
            {
                context.Notifications.Add(new Notification()
                {
                    UserId = item.ParticipantId,
                    ContestId = item.ContestId,
                    Message = String.Format(Constants.Notification.FINISHED_CONTEST_TEMPLATE, contest.Title)
                });
            }

            // notificate jury for finish
            foreach (var item in contest.Jury)
            {
                context.Notifications.Add(new Notification()
                {
                    UserId = item.JuryId,
                    ContestId = item.ContestId,
                    Message = String.Format(Constants.Notification.FINISHED_CONTEST_TEMPLATE, contest.Title)
                });
            }
        }

        /// <summary>
        /// Notificates that phase two starts.
        /// </summary>
        /// <param name="contest">The contest.</param>
        public void NotificatePhaseTwoStarts(Contest contest)
        {
            // notificate participants for finish
            foreach (var participant in contest.Participants)
            {
                context.Notifications.Add(new Notification()
                {
                    UserId = participant.ParticipantId,
                    ContestId = participant.ContestId,
                    Message = String.Format(Constants.Notification.PHASE_TWO_STARTS_TEMPLATE, contest.Title)
                });
            }

            // notificate jury for finish
            foreach (var jury in contest.Jury)
            {
                context.Notifications.Add(new Notification()
                {
                    UserId = jury.JuryId,
                    ContestId = jury.ContestId,
                    Message = String.Format(Constants.Notification.PHASE_TWO_STARTS_TEMPLATE, contest.Title)
                });
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Adds the admins in jury.
        /// </summary>
        /// <param name="contest">The contest.</param>
        private async Task AddAdminsInJury(Contest contest)
        {
            List<Guid> adminsIds = await context.Users.Where(u => u.IsAdmin)
                .Select(a => a.Id).ToListAsync();

            foreach (Guid adminId in adminsIds)
            {
                contest.Jury.Add(new ContestJury() { ContestId = contest.Id, JuryId = adminId });
            }
        }

        /// <summary>
        /// Adds the invited participants.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="contest">The contest.</param>
        private async Task AddInvitedParticipants(ContestDTO model, Contest contest)
        {
            foreach (string username in model.ParticipantsUsernames)
            {
                User user = await context.Users
                    .FirstOrDefaultAsync(u => u.UserName == username && !u.IsAdmin);

                if (user == null)
                {
                    continue;
                }

                Guid userId = user.Id;
                contest.Participants.Add(new ContestParticipant() { ContestId = contest.Id, ParticipantId = userId });
                user.Points += 3;

                context.Notifications.Add(new Notification()
                {
                    Contest = contest,
                    UserId = userId,
                    Message = String.Format(Constants.Notification.PARTICIPANT_INVITATION_TEMPLATE, contest.Title)
                });
            }
        }

        /// <summary>
        /// Adds the participants in jury.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="contest">The contest.</param>
        private async Task AddParticipantsInJury(ContestDTO model, Contest contest)
        {
            if (model.MasterCanBeJury)
            {
                foreach (string juryUsername in model.JuryUsernames)
                {
                    User user = await context.Users
                        .FirstOrDefaultAsync(u => u.UserName == juryUsername && !u.IsAdmin && u.Points >= Constants.Participant.MIN_POINTS_MASTER);

                    if (user == null)
                    {
                        continue;
                    }

                    Guid userId = user.Id;
                    contest.Jury.Add(new ContestJury() { ContestId = contest.Id, JuryId = userId });

                    context.Notifications.Add(new Notification()
                    {
                        Contest = contest,
                        UserId = userId,
                        Message = String.Format(Constants.Notification.JURY_INVITATION_TEMPLATE, contest.Title)
                    });
                }
            }
            else
            {
                foreach (string juryUsername in model.JuryUsernames)
                {
                    User user = await context.Users
                        .FirstOrDefaultAsync(u => u.UserName == juryUsername && !u.IsAdmin && u.Points >= Constants.Participant.MIN_POINTS_DICTATOR);

                    if (user == null)
                    {
                        continue;
                    }

                    Guid userId = user.Id;
                    contest.Jury.Add(new ContestJury() { ContestId = contest.Id, JuryId = userId });

                    context.Notifications.Add(new Notification()
                    {
                        Contest = contest,
                        UserId = userId,
                        Message = String.Format(Constants.Notification.JURY_INVITATION_TEMPLATE, contest.Title)
                    });
                }
            }
        }

        /// <summary>
        /// Gets the contests by phase include creator.
        /// </summary>
        /// <param name="phase">The phase.</param>
        /// <returns>Collection of contests</returns>
        private async Task<ICollection<Contest>> GetContestsByPhaseIncludeCreator(string phase)
        {
            ICollection<Contest> contests = new List<Contest>();

            contests = phase.ToLower() switch
            {
                "one" => await context.Contests
                                       .Include(c => c.Category)
                                       .Include(c => c.Creator)
                                       .Where(c => c.Phase == Constants.Contest.PHASE_ONE_STRING).ToListAsync(),
                "two" => await context.Contests
                                        .Include(c => c.Category)
                                        .Include(c => c.Creator)
                                        .Where(c => c.Phase == Constants.Contest.PHASE_TWO_STRING).ToListAsync(),
                "finished" => await context.Contests
                                        .Include(c => c.Category)
                                        .Include(c => c.Creator)
                                        .Where(c => c.Phase == Constants.Contest.PHASE_FINISHED_STRING).ToListAsync(),
                _ => throw new ArgumentException(Constants.Exception.INVALID_CONTEST_STATUS),
            };
            return contests;
        }

        /// <summary>
        /// Sets the cover.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="contest">The contest.</param>
        private async Task SetCover(ContestDTO model, Contest contest)
        {
            if (model.CoverId != Guid.Empty)
            {
                contest.CoverId = model.CoverId;
            }
            else if (model.CoverFile != null)
            {
                var uploadResult = await cloudinaryService.UploadCoverAsync(model.CoverFile);
                contest.Cover = new Image { CloudinaryId = uploadResult.PublicId, ImageUrl = uploadResult.Url.ToString() };
            }
            else if (model.CoverURL != null)
            {
                var uploadResult = await cloudinaryService.UploadCoverAsync(model.CoverURL);
                contest.Cover = new Image { CloudinaryId = uploadResult.PublicId, ImageUrl = uploadResult.Url.ToString() };
            }
            else
            {
                contest.CoverId = Guid.Parse("D606614E-C7CB-4BC0-B235-7FF10BE1C491");
            }
        }

        /// <summary>
        /// Filters contests by search filter.
        /// </summary>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="contests">The contests.</param>
        /// <returns>Search Filter</returns>
        private static IQueryable<Contest> SearchFilter(string searchFilter, IQueryable<Contest> contests)
        {
            if (searchFilter != null)
            {
                contests = contests.Where(c => c.Title.ToLower().Contains(searchFilter.ToLower()));
            }

            return contests;
        }

        /// <summary>
        /// Sorts the contests.
        /// </summary>
        /// <param name="contests">The contests.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="order">The order.</param>
        /// <returns>Collection of contests</returns>
        private ICollection<Contest> SortContests(ICollection<Contest> contests, SortBy? sortBy, OrderBy? order)
        {
            Func<Contest, object> sorter = GetSorter(sortBy);

            if (sortBy != null && order == null)
            {
                order = OrderBy.Asc;
            }

            if (order == OrderBy.Asc)
            {
                return contests.OrderBy(sorter).ToList();
            }
            else if (order == OrderBy.Desc)
            {
                return contests.OrderByDescending(sorter).ToList();
            }
            else
            {
                return contests;
            }
        }

        /// <summary>
        /// Gets the sorter.
        /// </summary>
        /// <param name="sortBy">The sort by.</param>
        /// <returns>Sorter</returns>
        private static Func<Contest, object> GetSorter(SortBy? sortBy)
        {
            return (contest) =>
            {
                return sortBy switch
                {
                    SortBy.Title => contest.Title,
                    SortBy.Category => contest.Category.Name,
                    SortBy.Creator => contest.Creator,
                    SortBy.Phase => contest.Phase,
                    SortBy.RemainingTime => contest.PhaseOneFinishTime - DateTime.Now,
                    _ => contest.Id
                };
            };
        }

        #endregion
    }
}
