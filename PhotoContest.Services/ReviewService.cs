﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PhotoContest.Services
{

    public class ReviewService : IReviewService
    {
        #region Fields and Constructor

        private readonly PhotoContestDbContext context;

        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReviewService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public ReviewService(PhotoContestDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates review asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public async Task CreateAsync(ReviewDTO model)
        {
            Contest contest = await context.Contests.FirstOrDefaultAsync(c => c.Id == model.ContestId);

            if (contest.Phase != Constants.Contest.PHASE_TWO_STRING)
                throw new ArgumentOutOfRangeException(Constants.Exception.INVALID_PHASE_REVIEWING);

            Review review = mapper.Map<Review>(model);

            await context.Reviews.AddAsync(review);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets reviews by photo asynchronous.
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        /// <param name="contestTitle">The contest title.</param>
        /// <returns>Collection of ReviewDTO</returns>
        /// <exception cref="FileNotFoundException"></exception>
        public async Task<ICollection<ReviewDTO>> GetByPhotoAsync(Guid photoId, string contestTitle)
        {
            Contest contest = await context.Contests
                .Include(c => c.Photos)
                .FirstOrDefaultAsync(c => c.Title == contestTitle);

            Photo photo = await context.Photos
                .Include(p => p.Reviews)
                    .ThenInclude(r => r.Author)
                .FirstOrDefaultAsync(p => p.Id == photoId)
                ?? throw new FileNotFoundException(Constants.Exception.INVALID_PHOTO_ID);

            HashSet<Review> photoReviews = photo.Reviews;
            ICollection<ReviewDTO> reviewDTOs = mapper.Map<ICollection<ReviewDTO>>(photoReviews);

            return reviewDTOs;
        }

        #endregion
    }
}
