﻿using AutoMapper;
using CloudinaryDotNet.Actions;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class PhotoService : IPhotoService
    {
        #region Fields and Constructor

        private readonly PhotoContestDbContext context;

        private readonly IMapper mapper;

        private readonly ICloudinaryService cloudinaryService;

        private readonly IContestService contestService;

        private readonly IValidatorService validatorService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhotoService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="cloudinaryService">The cloudinary service.</param>
        /// <param name="contestService">The contest service.</param>
        /// <param name="validatorService">The validator service.</param>
        public PhotoService(PhotoContestDbContext context,
            IMapper mapper, ICloudinaryService cloudinaryService,
            IContestService contestService,
            IValidatorService validatorService)
        {
            this.context = context;
            this.mapper = mapper;
            this.cloudinaryService = cloudinaryService;
            this.contestService = contestService;
            this.validatorService = validatorService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds the photo asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="contestTitle">The contest title.</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public async Task AddPhotoAsync(PhotoDTO model, string contestTitle)
        {
            if (!await validatorService.CheckParticipantAsync(contestTitle, model.AuthorId))
                throw new ArgumentException(Constants.Exception.UNAUTHORIZED_ACCESS);

            Contest contest = await context.Contests
                .FirstOrDefaultAsync(c => c.Title == contestTitle);
            if (contest.Phase != Constants.Contest.PHASE_ONE_STRING)
                throw new ArgumentOutOfRangeException(Constants.Exception.INVALID_PHASE_UPLOADING);

            if (await validatorService.CheckAlreadyUploadedPhotoAsync(contestTitle, model.AuthorId))
                throw new InvalidOperationException(Constants.Exception.ALREADY_UPLOADED_PHOTO);

            ImageUploadResult imageUploadResult = await cloudinaryService.UploadPhotoAsync(model.File);

            Photo photo = mapper.Map<Photo>(model);
            photo.ImageUrl = imageUploadResult.Url.ToString();
            photo.CloudinaryId = imageUploadResult.PublicId;

            contest.Photos.Add(new ContestPhoto() { ContestId = contest.Id, Photo = photo });

            await context.Photos.AddAsync(photo);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the photo asynchronous.
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        public async Task DeletePhotoAsync(Guid photoId)
        {
            Photo photo = await context.Photos.FirstOrDefaultAsync(p => p.Id == photoId);
            photo.IsDeleted = true;

            await context.SaveChangesAsync();
        }

        //public async Task DeleteImageAsync(Guid imageId)
        //{                                         //TODO: What if photo is null
        //    Image image = await context.Images.FirstOrDefaultAsync(p => p.Id == imageId);
        //    image.IsDeleted = true;

        //    await context.SaveChangesAsync();
        //}

        //public async Task<ICollection<PhotoDTO>> GetPhotosAsync()
        //{
        //    ICollection<Photo> photos = await context.Photos.ToListAsync();
        //    ICollection<PhotoDTO> photosDTOs = mapper.Map<ICollection<PhotoDTO>>(photos);

        //    return photosDTOs;
        //}

        /// <summary>
        /// Gets the images asynchronous.
        /// </summary>
        /// <returns>Collection of ImageDTO</returns>
        public async Task<ICollection<ImageDTO>> GetImagesAsync()
        {
            ICollection<Image> images = await context.Images.ToListAsync();
            ICollection<ImageDTO> imagesDTOs = mapper.Map<ICollection<ImageDTO>>(images);

            return imagesDTOs;
        }

        /// <summary>
        /// Gets the photo asynchronous.
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        /// <returns>Collection of PhotoDTO</returns>
        public async Task<PhotoDTO> GetPhotoAsync(Guid photoId)
        {
            Photo photo = await context.Photos
                .Include(p => p.Author)
                .FirstOrDefaultAsync(p => p.Id == photoId);
            PhotoDTO photoDTO = mapper.Map<PhotoDTO>(photo);

            return photoDTO;
        }

        /// <summary>
        /// Gets the latest winning photos asynchronous.
        /// </summary>
        /// <returns>Collection of PhotoDTO</returns>
        public async Task<ICollection<PhotoDTO>> GetLatestWinningPhotosAsync()
        {
            var topPhotos = await context.Photos
                .OrderByDescending(p => p.Reviews.Sum(p => p.Score))
                .Take(6)
                .ToListAsync();

            var topPhotosDTOs = mapper.Map<ICollection<PhotoDTO>>(topPhotos);

            return topPhotosDTOs;
        }

        /// <summary>
        /// Gets the photos by participant asynchronous.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>Collection of PhotoDTO</returns>
        public async Task<ICollection<PhotoDTO>> GetPhotosByParticipantAsync(string username)
        {
            var photos = await context.Photos
                .Include(p => p.Author)
                .Where(p => p.Author.UserName == username)
                .ToListAsync();

            var photosDTOs = mapper.Map<ICollection<PhotoDTO>>(photos);

            return photosDTOs;
        }

        /// <summary>
        /// Gets photo by contest and author asynchronous.
        /// </summary>
        /// <param name="authorId">The author identifier.</param>
        /// <param name="contestTitle">The contest title.</param>
        /// <returns>PhotoDTO</returns>
        public async Task<PhotoDTO> GetByContestAndAuthorAsync(Guid authorId, string contestTitle)
        {
            Photo photo = await context.Photos
                .Include(p => p.Contests)
                .ThenInclude(c => c.Contest)
                .FirstOrDefaultAsync(p => p.AuthorId == authorId && p.Contests.Any(c => c.Contest.Title == contestTitle));

            PhotoDTO photoDTO = mapper.Map<PhotoDTO>(photo);

            return photoDTO;
        }

        /// <summary>
        /// Gets the by contest asynchronous.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <returns>Collection of PhotoDTO</returns>
        public async Task<ICollection<PhotoDTO>> GetByContestAsync(string contestTitle)
        {
            Contest contest = await context.Contests
                .Include(c => c.Photos)
                .Include(c => c.Jury)
                .FirstOrDefaultAsync(c => c.Title == contestTitle);

            List<Photo> photos = await context.Photos
                .Include(p => p.Contests)
                    .ThenInclude(c => c.Contest)
                .Include(x => x.Author)
                .Where(p => p.Contests.Any(cp => cp.ContestId == contest.Id))
                .ToListAsync();

            ICollection<PhotoDTO> photosDTOs = mapper.Map<ICollection<PhotoDTO>>(photos);

            return photosDTOs;
        }

        /// <summary>
        /// Gets the total points asynchronous.
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        /// <returns>Total points for the photo</returns>
        public async Task<int> GetTotalPointsAsync(Guid photoId)
        {
            Photo photo = await context.Photos
                .Include(p => p.Reviews)
                .FirstOrDefaultAsync(p => p.Id == photoId);

            return photo.Reviews.Sum(r => r.Score);
        }

        #endregion
    }
}
