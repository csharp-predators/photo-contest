﻿using PhotoContest.Data.Models;
using System.Collections.Generic;

namespace PhotoContest.Services.DTOs
{

    public class StandingsList
    {
        /// <summary>
        /// Gets or sets the first placed.
        /// </summary>
        public List<ContestPhoto> FirstPlaced { get; set; }
        /// <summary>
        /// Gets or sets the second paced.
        /// </summary>
        public List<ContestPhoto> SecondPaced { get; set; }
        /// <summary>
        /// Gets or sets the third placed.
        /// </summary>
        public List<ContestPhoto> ThirdPlaced { get; set; }
        /// <summary>
        /// Gets or sets the low placed.
        /// </summary>
        public List<ContestPhoto> LowPlaced { get; set; }
    }
}
