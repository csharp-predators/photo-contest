﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.DTOs
{

    public class AuthMessageSenderOptions
    {
        /// <summary>
        /// Gets or sets the send grid user.
        /// </summary>
        public string SendGridUser { get; set; }

        /// <summary>
        /// Gets or sets the send grid key.
        /// </summary>
        public string SendGridKey { get; set; }
    }
}
