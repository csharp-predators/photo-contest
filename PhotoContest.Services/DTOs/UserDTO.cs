﻿using Microsoft.AspNetCore.Http;
using System;

namespace PhotoContest.Services.DTOs
{
    public class UserDTO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDTO"/> class.
        /// </summary>
        /// <param name="jwtToken">The JWT token.</param>
        public UserDTO(string jwtToken)
        {
            JwtToken = jwtToken;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDTO"/> class.
        /// </summary>
        public UserDTO()
        {

        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the avatar file.
        /// </summary>
        public IFormFile AvatarFile { get; set; }

        /// <summary>
        /// Gets or sets the avatar identifier.
        /// </summary>
        public Guid AvatarId { get; set; }
        /// <summary>
        /// Gets or sets the avatar.
        /// </summary>
        public ImageDTO Avatar { get; set; }

        /// <summary>
        /// Gets or sets the avatar path.
        /// </summary>
        public string AvatarPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is admin.
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Gets or sets the points.
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets the points to next rank.
        /// </summary>
        public int PointsToNextRank { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public string Rank { get; set; }

        /// <summary>
        /// Gets or sets the JWT token.
        /// </summary>
        public string JwtToken { get; set; }
    }
}
