﻿using Microsoft.AspNetCore.Http;
using System;

namespace PhotoContest.Services.DTOs
{

    public class PhotoDTO
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets the cloudinary identifier.
        /// </summary>
        public string CloudinaryId { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        public IFormFile File { get; set; }

        /// <summary>
        /// Gets or sets the author identifier.
        /// </summary>
        public Guid AuthorId { get; set; }

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        public UserDTO Author { get; set; }
    }
}
