﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace PhotoContest.Services.DTOs
{

    public class ContestDTO
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the category identifier.
        /// </summary>
        public int CategoryId { get; set; }
        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public CategoryDTO Category { get; set; }

        /// <summary>
        /// Gets or sets the creator identifier.
        /// </summary>
        public Guid CreatorId { get; set; }

        /// <summary>
        /// Gets or sets the creator.
        /// </summary>
        public UserDTO Creator { get; set; }

        /// <summary>
        /// Gets or sets the cover identifier.
        /// </summary>
        public Guid CoverId { get; set; }

        /// <summary>
        /// Gets or sets the cover.
        /// </summary>
        public ImageDTO Cover { get; set; }

        /// <summary>
        /// Gets or sets the cover file.
        /// </summary>
        public IFormFile CoverFile { get; set; }

        /// <summary>
        /// Gets or sets the cover URL.
        /// </summary>
        public string CoverURL { get; set; }

        /// <summary>
        /// Gets or sets the phase.
        /// </summary>
        public string Phase { get; set; }

        /// <summary>
        /// Gets or sets the phase one finish time.
        /// </summary>
        public DateTime PhaseOneFinishTime { get; set; }

        /// <summary>
        /// Gets or sets the phase two finish time.
        /// </summary>
        public DateTime PhaseTwoFinishTime { get; set; }

        /// <summary>
        /// Gets the remaining time phase one.
        /// </summary>
        public TimeSpan RemainingTimePhaseOne => PhaseOneFinishTime - DateTime.Now;

        /// <summary>
        /// Gets the remaining time phase two.
        /// </summary>
        public TimeSpan RemainingTimePhaseTwo => PhaseTwoFinishTime - DateTime.Now;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is invitational.
        /// </summary>
        public bool IsInvitational { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [master can be jury].
        /// </summary>
        public bool MasterCanBeJury { get; set; }

        /// <summary>
        /// Gets or sets the participants usernames.
        /// </summary>
        public ICollection<string> ParticipantsUsernames { get; set; }

        /// <summary>
        /// Gets or sets the jury usernames.
        /// </summary>
        public ICollection<string> JuryUsernames { get; set; }
    }
}
