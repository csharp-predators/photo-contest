﻿using System;

namespace PhotoContest.Services.DTOs
{

    public class ReviewDTO
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public int? Score { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the fits category.
        /// </summary>
        public bool? FitsCategory { get; set; }

        /// <summary>
        /// Gets or sets the author identifier.
        /// </summary>
        public Guid AuthorId { get; set; }
        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        public UserDTO Author { get; set; }

        /// <summary>
        /// Gets or sets the contest identifier.
        /// </summary>
        public Guid ContestId { get; set; }

        /// <summary>
        /// Gets or sets the photo identifier.
        /// </summary>
        public Guid PhotoId { get; set; }
    }
}
