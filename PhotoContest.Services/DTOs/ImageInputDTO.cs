﻿using Microsoft.AspNetCore.Http;

namespace PhotoContest.Services.DTOs
{
    public class ImageInputDTO
    {
        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        public IFormFile File { get; set; }
    }
}
