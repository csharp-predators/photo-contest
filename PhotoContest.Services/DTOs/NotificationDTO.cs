﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.DTOs
{

    public class NotificationDTO
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public UserDTO User { get; set; }

        /// <summary>
        /// Gets or sets the contest identifier.
        /// </summary>
        public Guid ContestId { get; set; }

        /// <summary>
        /// Gets or sets the contest.
        /// </summary>
        public ContestDTO Contest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is readed.
        /// </summary>
        public bool IsReaded { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}
