﻿namespace PhotoContest.Services.DTOs
{

    public class CategoryDTO
    {
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}
