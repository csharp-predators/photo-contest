﻿using AutoMapper;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Helpers;
using PhotoContest.Services.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services
{

    public class UserService : IUserService
    {
        #region Fields and Constructor

        private readonly UserManager<User> userManager;

        private readonly PhotoContestDbContext context;

        private readonly IMapper mapper;

        private readonly ICloudinaryService cloudinaryService;

        private readonly AppSettings appSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="appSettings">The application settings.</param>
        /// <param name="cloudinaryService">The cloudinary service.</param>
        public UserService(UserManager<User> userManager,
            PhotoContestDbContext context,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            ICloudinaryService cloudinaryService)
        {
            this.userManager = userManager;
            this.context = context;
            this.mapper = mapper;
            this.cloudinaryService = cloudinaryService;
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UserService(PhotoContestDbContext context)
        {
            this.context = context;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Logins user asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>UserDTO</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<UserDTO> LoginAsync(LoginDTO model)
        {
            User user = await context.Users.SingleOrDefaultAsync(x => x.UserName == model.UserName);
            bool isCorrectPass = await userManager.CheckPasswordAsync(user, model.Password);

            if (user == null || !isCorrectPass)
            {
                throw new ArgumentException(Constants.Exception.INVALID_LOGIN);
            }

            UserDTO userDTO = mapper.Map<UserDTO>(user);
            string jwtToken = await GenerateJwtToken(user);
            userDTO.JwtToken = jwtToken;

            return userDTO;
        }

        /// <summary>
        /// Creates user asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="password">The password.</param>
        /// <returns>UserDTO</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<UserDTO> CreateAsync(UserDTO model, string password)
        {
            User user = mapper.Map<User>(model);
            user.CreatedOn = DateTime.Now;

            if (model.AvatarFile == null)
            {
                Image avatar = await context.Images.FirstOrDefaultAsync();
                user.AvatarId = avatar.Id;
            }
            else
            {
                CloudinaryDotNet.Actions.ImageUploadResult uploadResult = await cloudinaryService.UploadAvatarAsync(model.AvatarFile);
                user.Avatar = new Image { ImageUrl = uploadResult.Url.ToString(), CloudinaryId = uploadResult.PublicId };
            }

            user.SecurityStamp = Guid.NewGuid().ToString();

            IdentityResult createResult = await userManager.CreateAsync(user, password);

            if (createResult.Succeeded)
            {
                await userManager.AddToRoleAsync(user, "Junkie");
            }
            else
            {
                throw new ArgumentException();
            }

            string jwtToken = await GenerateJwtToken(user);
            UserDTO userDTO = mapper.Map<UserDTO>(user);
            userDTO.JwtToken = jwtToken;

            return userDTO;
        }

        /// <summary>
        /// Gets users asynchronous.
        /// </summary>
        /// <returns>Collection of UserDTO</returns>
        public async Task<ICollection<UserDTO>> GetAsync()
        {
            ICollection<User> users = await context.Users
                .Include(u => u.Avatar)
                .ToListAsync();
            ICollection<UserDTO> userDTOs = mapper.Map<ICollection<UserDTO>>(users);

            return userDTOs;
        }

        /// <summary>
        /// Gets user asynchronous.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>UserDTO</returns>
        public async Task<UserDTO> GetAsync(string username)
        {
            User user = await context.Users
                .Include(u => u.Avatar)
                .FirstOrDefaultAsync(u => u.UserName == username);
            UserDTO userDTO = mapper.Map<UserDTO>(user);

            userDTO.PointsToNextRank = CalculatePointsToNextRank(userDTO.Points);

            return userDTO;
        }

        /// <summary>
        /// Gets user by id asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>UserDTO</returns>
        public async Task<UserDTO> GetAsync(Guid userId)
        {
            User user = await context.Users
                .Include(u => u.Avatar)
                .FirstOrDefaultAsync(u => u.Id == userId);
            UserDTO userDTO = mapper.Map<UserDTO>(user);

            userDTO.PointsToNextRank = CalculatePointsToNextRank(userDTO.Points);

            return userDTO;
        }

        /// <summary>
        /// Gets the participants asynchronous.
        /// </summary>
        /// <returns>Collection of UserDTO</returns>
        public async Task<ICollection<UserDTO>> GetParticipantsAsync()
        {
            ICollection<User> users = await context.Users.Where(u => !u.IsAdmin).OrderBy(p => p.Points).ToListAsync();
            ICollection<UserDTO> userDTOs = mapper.Map<ICollection<UserDTO>>(users);

            return userDTOs;
        }

        /// <summary>
        /// Gets the participants asynchronous.
        /// </summary>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        /// <returns>Collection of UserDTO</returns>
        public async Task<ICollection<UserDTO>> GetParticipantsAsync(string searchFilter, SortByUser? sortBy, OrderBy? orderBy)
        {
            IQueryable<User> query = context.Users.Where(u => !u.IsAdmin)
                .Include(u => u.Avatar)
                .OrderBy(p => p.Points)
                .AsQueryable();

            List<User> users = await SearchFilter(searchFilter, query).ToListAsync();
            users = SortUsers(users, sortBy, orderBy).ToList();

            ICollection<UserDTO> filteredParticipants = mapper.Map<ICollection<UserDTO>>(users);

            return filteredParticipants;
        }

        /// <summary>
        /// Updates user asynchronous.
        /// </summary>
        /// <param name="participantName">Name of the participant.</param>
        /// <param name="model">The model.</param>
        public async Task UpdateAsync(string participantName, UserDTO model)
        {
            //TODO: what if user is null
            User user = await context.Users.FirstOrDefaultAsync(u => u.UserName == participantName);

            if (!string.IsNullOrEmpty(model.FirstName))
            {
                user.FirstName = model.FirstName;
            }

            if (!string.IsNullOrEmpty(model.LastName))
            {
                user.LastName = model.LastName;
            }

            if (!string.IsNullOrEmpty(model.Email))
            {
                user.Email = model.Email;
            }

            if (model.AvatarFile != null)
            {
                ImageUploadResult uploadresult = await cloudinaryService.UploadAvatarAsync(model.AvatarFile);
                user.Avatar = new Image { CloudinaryId = uploadresult.PublicId, ImageUrl = uploadresult.Url.ToString() };
            }

            await context.SaveChangesAsync();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Generates the JWT token.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>JWT token</returns>
        private async Task<string> GenerateJwtToken(User user)
        {
            IList<string> roles = await userManager.GetRolesAsync(user);
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(appSettings.Secret);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(31),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            foreach (string role in roles)
            {
                tokenDescriptor.Subject.AddClaim(new Claim(ClaimTypes.Role, role));
            }

            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        /// <summary>
        /// Calculates the points to next rank.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <returns>Points to next rank</returns>
        private static int CalculatePointsToNextRank(int points)
        {
            int pointsToNextRank = 0;

            if (points < Constants.Participant.MIN_POINTS_ENTHUSIAST)
            {
                pointsToNextRank = Constants.Participant.MIN_POINTS_ENTHUSIAST - points;
            }
            else if (points < Constants.Participant.MIN_POINTS_MASTER)
            {
                pointsToNextRank = Constants.Participant.MIN_POINTS_MASTER - points;
            }
            else if (points < Constants.Participant.MIN_POINTS_DICTATOR)
            {
                pointsToNextRank = Constants.Participant.MIN_POINTS_DICTATOR - points;
            }

            return pointsToNextRank;
        }

        /// <summary>
        /// Filters users bu search filter.
        /// </summary>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="query">The query.</param>
        /// <returns>Search Filter</returns>
        private static IQueryable<User> SearchFilter(string searchFilter, IQueryable<User> query)
        {
            if (searchFilter != null)
            {
                query = query.Where(u => u.UserName.ToLower().Contains(searchFilter.ToLower()));
            }

            return query;
        }

        /// <summary>
        /// Sorts the users.
        /// </summary>
        /// <param name="users">The users.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="order">The order.</param>
        /// <returns>Collection of users</returns>
        private ICollection<User> SortUsers(ICollection<User> users, SortByUser? sortBy, OrderBy? order)
        {
            Func<User, object> sorter = GetSorter(sortBy);

            if (sortBy != null && order == null)
            {
                order = OrderBy.Asc;
            }

            if (order == OrderBy.Asc)
            {
                return users.OrderBy(sorter).ToList();
            }
            else if (order == OrderBy.Desc)
            {
                return users.OrderByDescending(sorter).ToList();
            }
            else
            {
                return users;
            }
        }

        /// <summary>
        /// Gets the sorter.
        /// </summary>
        /// <param name="sortBy">The sort by.</param>
        /// <returns>Sorter</returns>
        private static Func<User, object> GetSorter(SortByUser? sortBy)
        {
            return (user) =>
            {
                return sortBy switch
                {
                    SortByUser.FirstName => user.FirstName,
                    SortByUser.LastName => user.LastName,
                    SortByUser.Points => user.Points,
                    SortByUser.Rank => user.Rank,
                    SortByUser.UserName => user.UserName,
                    _ => user.Id
                };
            };
        }

        #endregion
    }
}
