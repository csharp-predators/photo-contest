﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class NotificationService : INotificationService
    {

        private readonly PhotoContestDbContext context;

        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public NotificationService(PhotoContestDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets the user notifications asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Collection of NotificationDTO</returns>
        public async Task<ICollection<NotificationDTO>> GetUserNotificationsAsync(Guid userId)
        {
            List<Data.Models.Notification> notifications = await context.Notifications
                .Include(n => n.Contest)
                .Include(n => n.User)
                .Where(n => n.UserId == userId && !n.IsReaded)
                .ToListAsync();
            ICollection<NotificationDTO> notificationsDTOs = mapper.Map<ICollection<NotificationDTO>>(notifications);

            return notificationsDTOs;
        }

        /// <summary>
        /// Marks the notication as readed asynchronous.
        /// </summary>
        /// <param name="notificationId">The notification identifier.</param>
        public async Task MarkNoticationAsReadedAsync(Guid notificationId)
        {
            Data.Models.Notification notificationToMark = await context.Notifications.FirstOrDefaultAsync(n => n.Id == notificationId);

            notificationToMark.IsReaded = true;

            await context.SaveChangesAsync();
        }
    }
}
