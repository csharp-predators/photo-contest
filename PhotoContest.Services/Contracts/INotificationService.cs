﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface INotificationService
    {
        Task<ICollection<NotificationDTO>> GetUserNotificationsAsync(Guid userId);

        Task MarkNoticationAsReadedAsync(Guid notificationId);
    }
}
