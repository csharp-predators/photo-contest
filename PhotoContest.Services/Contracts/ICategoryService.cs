﻿using PhotoContest.Services.DTOs;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface ICategoryService
    {
        Task<CategoryDTO> GetAsync(string categoryName);

        Task<CategoryDTO> GetCategoryAsync(string categoryName);

        Task CreateAsync(CategoryDTO model);
    }
}
