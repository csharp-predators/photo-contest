﻿using PhotoContest.Services.DTOs;
using PhotoContest.Services.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IUserService
    {
        Task<UserDTO> LoginAsync(LoginDTO model);

        Task<UserDTO> CreateAsync(UserDTO model, string password);

        Task<UserDTO> GetAsync(string username);

        Task<UserDTO> GetAsync(Guid userId);

        Task<ICollection<UserDTO>> GetAsync();

        Task<ICollection<UserDTO>> GetParticipantsAsync();

        Task<ICollection<UserDTO>> GetParticipantsAsync(string searchFilter, SortByUser? sortBy, OrderBy? orderBy);

        Task UpdateAsync(string participantName, UserDTO model);
    }
}
