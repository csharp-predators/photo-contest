﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IReviewService
    {
        Task<ICollection<ReviewDTO>> GetByPhotoAsync(Guid photoId, string contestTitle);

        Task CreateAsync(ReviewDTO model);
    }
}
