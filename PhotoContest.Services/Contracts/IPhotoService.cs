﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IPhotoService
    {
        Task<PhotoDTO> GetPhotoAsync(Guid photoId);

        Task<ICollection<PhotoDTO>> GetLatestWinningPhotosAsync();

        //Task<ICollection<PhotoDTO>> GetPhotosAsync();

        Task<ICollection<ImageDTO>> GetImagesAsync();

        Task<ICollection<PhotoDTO>> GetPhotosByParticipantAsync(string username);

        Task<PhotoDTO> GetByContestAndAuthorAsync(Guid authorId, string contestTitle);

        Task<int> GetTotalPointsAsync(Guid photoId);

        Task AddPhotoAsync(PhotoDTO model, string contestTitle);

        Task DeletePhotoAsync(Guid photoId);

        //Task DeleteImageAsync(Guid imageId);

        Task<ICollection<PhotoDTO>> GetByContestAsync(string contestTitle);
    }
}
