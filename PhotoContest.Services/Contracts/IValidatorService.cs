﻿using System;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IValidatorService
    {
        Task<bool> CheckAdminAsync(Guid userId);

        Task<bool> CheckAlreadyUploadedPhotoAsync(string contestTitle, Guid participantId);

        Task<bool> CheckJuryAsync(string contestTitle, Guid participantId);

        Task<bool> CheckParticipantAsync(string contestTitle, Guid participantId);

        Task<bool> CheckPhotoAuthorAsync(Guid photoId, Guid userId);

        bool ValidGuidString(string guidString);

        bool CheckAlreadyReviewedPhotoAsync(Guid photoId, Guid juryId);
    }
}
