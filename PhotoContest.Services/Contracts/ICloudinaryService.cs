﻿using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface ICloudinaryService
    {
        Task<ImageUploadResult> UploadPhotoAsync(IFormFile file);

        Task<ImageUploadResult> UploadAvatarAsync(IFormFile file);

        Task<ImageUploadResult> UploadCoverAsync(string url);

        Task<ImageUploadResult> UploadCoverAsync(IFormFile file);
    }
}
