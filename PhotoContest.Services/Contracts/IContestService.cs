﻿using PhotoContest.Data.Models;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IContestService
    {
        Task<ContestDTO> GetAsync(string contestTitle);

        Task<ICollection<ContestDTO>> GetByPhaseAsync(string phase);

        Task<ICollection<ContestDTO>> GetByPhaseAsync(string phase, string? searchFilter, SortBy? sortBy, OrderBy? orderBy);

        Task<ICollection<ContestDTO>> GetAllAsync(string searchFilter, SortBy? sortBy, OrderBy? orderBy);

        Task<ICollection<ContestDTO>> GetByParticipantAndStatusAsync(Guid participantId, string status, string search, SortBy? sortBy, OrderBy? orderBy);

        Task<ICollection<ContestDTO>> GetByParticipantAndStatusAsync(Guid participantId, string status);

        Task<ContestDTO> GetByPhotoAsync(Guid photoId);

        Task<ICollection<ContestDTO>> GetByJuryAsync(Guid juryId, string search, SortBy? sortBy, OrderBy? orderBy);

        Task CreateAsync(ContestDTO model);

        Task AddParticipantAsync(Guid userId, string contestTitle);

        void AwardWinnersPoints(string contestTitle);

        Task<StandingsList> GetWinners(string contestTitle);

        void NotificateFinish(Contest contest);
        void NotificatePhaseTwoStarts(Contest contest);
    }
}
