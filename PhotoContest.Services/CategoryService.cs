﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="PhotoContest.Services.Contracts.ICategoryService" />
    public class CategoryService : ICategoryService
    {
        #region Fields and Constructor

        /// <summary>
        /// The context
        /// </summary>
        private readonly PhotoContestDbContext context;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public CategoryService(PhotoContestDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="model">Category model.</param>
        public async Task CreateAsync(CategoryDTO model)
        {
            Category category = mapper.Map<Category>(model);

            await context.Categories.AddAsync(category);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets the category asynchronous.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <returns>CategoryDTO</returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<CategoryDTO> GetAsync(string categoryName)
        {
            Category category = await context.Categories.FirstOrDefaultAsync(c => c.Name == categoryName)
                ?? throw new ArgumentException(Constants.Exception.INVALID_CONTEST_TITLE);

            CategoryDTO categoryDTO = mapper.Map<CategoryDTO>(category);

            return categoryDTO;
        }

        /// <summary>
        /// Gets the category asynchronous.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <returns>CategoryDTO</returns>
        public async Task<CategoryDTO> GetCategoryAsync(string categoryName)
        {
            Category category = await context.Categories.FirstOrDefaultAsync(c => c.Name == categoryName);

            CategoryDTO categoryDTO = mapper.Map<CategoryDTO>(category);

            return categoryDTO;
        }


        #endregion
    }
}
