﻿namespace PhotoContest.Services.Common
{
    public static class Constants
    {
        public static class Contest
        {
            public const string PHASE_ONE_STRING = "Phase One";
            public const string PHASE_TWO_STRING = "Phase Two";
            public const string PHASE_FINISHED_STRING = "Phase Finished";
        }

        public static class Participant
        {
            public const int MIN_POINTS_JUNKIE = 0;
            public const int MIN_POINTS_ENTHUSIAST = 51;
            public const int MIN_POINTS_MASTER = 151;
            public const int MIN_POINTS_DICTATOR = 1001;

            public const string RANK_JUNKIE = "Junkie";
            public const string RANK_Enthusiast = "Enthusiast";
            public const string RANK_MASTER = "Master";
            public const string RANK_Dictator = "Dictator";
        }

        public static class Exception
        {
            public const string INVALID_CONTEST_TITLE = "Invalid contest title.";
            public const string EXISTING_CONTEST_TITLE = "Given contest title already exist.";
            public const string INVALID_CONTEST_PHASE = "Invalid phase.";
            public const string INVALID_CONTEST_STATUS = "Invalid status. Must be either current or finished.";
            public const string ONLY_JURY_ACCESS = "Only jury can access thirs resourses.";
            public const string INVALID_PHOTO_ID = "Given photo id is not correct.";
            public const string INVALID_PHASE_REVIEWING = "Reviews can be added only in Phase Two.";
            public const string INVALID_PHASE_UPLOADING = "Reviews can be added only in Phase One.";
            public const string UNAUTHORIZED_ACCESS = "You have not access to this resource.";
            public const string ALREADY_UPLOADED_PHOTO = "You can upload only 1 photo per contest.";
            public const string INVALID_LOGIN = "Invalid password or username.";
            public const string INVALID_CONTEST_PARTICIPATING = "Given contest is not in Phase One. You can't add participants or join it.";
            public const string ALREADY_JOINED = "You already participate in this contest.";
            public static string NOT_JOINED = "You do not participate in this contests.";
        }

        public class Notification
        {
            public const string JURY_INVITATION_TEMPLATE = "You are invited for jury in contest \"{0}\"";
            public const string PARTICIPANT_INVITATION_TEMPLATE = "You are invited for participant in contest \"{0}\"";
            public const string FINISHED_CONTEST_TEMPLATE = "Contest \"{0}\" finished. Now you can see results";
            public const string PHASE_TWO_STARTS_TEMPLATE = "Contest \"{0}\" phase changed to Phase Two. Now jury can write reviews and score photos.";
        }
    }
}
