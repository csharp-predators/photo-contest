﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class GetAsyncById_Should
    {
        [TestMethod]
        public async Task Returns_CorrectUserDTO_WhenValidId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectUserDTO_WhenValidId));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            Image avatar = new Image()
            {
                CloudinaryId = "Testing",
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png"
            };

            var userId = Guid.NewGuid();
            User user = new User()
            {
                Id = userId,
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "testUsername",
                Email = "test@email.com",
                Avatar=avatar
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                UserDTO result = await sut.GetAsync(userId).ConfigureAwait(false);

                //Assert
                Assert.AreEqual("testUsername", result.UserName);
            }
        }
    }
}
