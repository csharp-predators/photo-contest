﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class LoginAsync_Should
    {
        [TestMethod]
        public async Task ThrowsArgumentExceptionWhenInvalidLogin()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowsArgumentExceptionWhenInvalidLogin));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            Mock<IMapper> mapper = new Mock<IMapper>();
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            PasswordHasher<User> passHasher = new PasswordHasher<User>();
            User user = new User()
            {
                UserName = "testUsername",
            };
            string password = passHasher.HashPassword(user, "testpass");
            user.PasswordHash = password;

            var loginDto = new LoginDTO()
            {
                UserName = "testUsername",
                Password = "wrongpassword"
            };

            userManager.Setup(x => x.CheckPasswordAsync(user, "wrongpassword")).ReturnsAsync(false);

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper.Object, appSettings.Object, cloudinaryService.Object);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.LoginAsync(loginDto));
            }
        }

        [TestMethod]
        public async Task ThrowsArgumentExceptionWhenInvalidLoginUserIsNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ThrowsArgumentExceptionWhenInvalidLoginUserIsNull));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            Mock<IMapper> mapper = new Mock<IMapper>();
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            var loginDto = new LoginDTO()
            {
                UserName = "testUsername",
                Password = "testpass"
            };

            using (var context = new PhotoContestDbContext(options))
            {
                UserService sut = new UserService(userManager.Object, context, mapper.Object, appSettings.Object, cloudinaryService.Object);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.LoginAsync(loginDto));
            }
        }

        [TestMethod]
        public async Task Returns_Correct_UserDTOModel()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_Correct_UserDTOModel));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            PasswordHasher<User> passHasher = new PasswordHasher<User>();
            User user = new User()
            {
                UserName = "testUsername",
            };
            string password = passHasher.HashPassword(user, "testpass");
            user.PasswordHash = password;

            var loginDto = new LoginDTO()
            {
                UserName = "testUsername",
                Password = "testpass"
            };

            AppSettings appSett = new AppSettings()
            {
                Secret = "THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING"
            };
            userManager.Setup(x => x.CheckPasswordAsync(user, "testpass")).ReturnsAsync(true);
            userManager.Setup(x => x.GetRolesAsync(user)).ReturnsAsync(new List<string>());
            appSettings.Setup(x => x.Value).Returns(appSett);

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                var userDTOResult = await sut.LoginAsync(loginDto);

                //Assert
                Assert.IsInstanceOfType(userDTOResult, typeof(UserDTO));
                Assert.AreEqual(userDTOResult.UserName, user.UserName);
            }
        }
    }
}
