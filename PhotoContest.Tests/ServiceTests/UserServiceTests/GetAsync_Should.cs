﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task Returns_CorrectCollection_WhenValidUsername()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectCollection_WhenValidUsername));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            Image avatar = new Image()
            {
                CloudinaryId = "Testing",
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png"
            };

            User user = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "testUsername",
                Email = "test@email.com",
                Avatar=avatar
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                context.Images.Add(avatar);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                var result = await sut.GetAsync();

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }
    }
}
