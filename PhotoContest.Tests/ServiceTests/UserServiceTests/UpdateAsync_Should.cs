﻿using AutoMapper;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class UpdateAsync_Should
    {
        [TestMethod]
        public async Task Returns_CorrectUpdateModel_FirstName()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectUpdateModel_FirstName));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();


            User user1 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "first",
                Email = "test@email.com",
            };

            UserDTO userDTO = new UserDTO()
            {
                FirstName = "changed"
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user1);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                await sut.UpdateAsync("first", userDTO);

                //Assert
                Assert.AreEqual("changed", context.Users.First().FirstName);
            }
        }

        [TestMethod]
        public async Task Returns_CorrectUpdateModel_LastName()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectUpdateModel_LastName));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();


            User user1 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "first",
                Email = "test@email.com",
            };

            UserDTO userDTO = new UserDTO()
            {
                LastName = "changed"
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user1);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                await sut.UpdateAsync("first", userDTO);

                //Assert
                Assert.AreEqual("changed", context.Users.First().LastName);
            }
        }

        [TestMethod]
        public async Task Returns_CorrectUpdateModel_Email()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectUpdateModel_Email));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();


            User user1 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "first",
                Email = "test@email.com",
            };

            UserDTO userDTO = new UserDTO()
            {
                Email = "changed"
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user1);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                await sut.UpdateAsync("first", userDTO);

                //Assert
                Assert.AreEqual("changed", context.Users.First().Email);
            }
        }

        [TestMethod]
        public async Task Returns_CorrectUpdateModel_AvatarFile()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectUpdateModel_Email));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            string path = @"../../../unnamed.jpg";
            using MemoryStream stream = new MemoryStream(File.ReadAllBytes(path).ToArray());
            FormFile formFile = new FormFile(stream, 0, stream.Length, "streamFile", "fileName");

            User user1 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "first",
                Email = "test@email.com",
            };

            UserDTO userDTO = new UserDTO()
            {
                AvatarFile = formFile
            };

            cloudinaryService.Setup(x => x.UploadAvatarAsync(formFile))
                .ReturnsAsync(new ImageUploadResult() 
                { PublicId = "test", Url =new Uri("https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png") });

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user1);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                await sut.UpdateAsync("first", userDTO);

                //Assert
                Assert.IsNotNull(context.Users.First().Email);
            }
        }
    }
}
