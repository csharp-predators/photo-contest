﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.Helpers;
using PhotoContest.Services.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class GetParticipantAsyncFiltration_Should
    {
        [TestMethod]
        public async Task Returns_CorrectCollection_WithoutFilter()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectCollection_WithoutFilter));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            Image avatar = new Image()
            {
                CloudinaryId = "Testing",
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png"
            };

            User user1 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "first",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 60
            };

            User user2 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "second",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 30
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user1);
                context.Users.Add(user2);
                context.Images.Add(avatar);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                var result = await sut.GetParticipantsAsync(null, null, null);

                //Assert
                Assert.AreEqual(2, result.Count);
                Assert.AreEqual(user2.UserName, result.First().UserName);
            }
        }

        [TestMethod]
        public async Task Returns_CorrectCollection_WithFiltrationSearch()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectCollection_WithFiltrationSearch));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            Image avatar = new Image()
            {
                CloudinaryId = "Testing",
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png"
            };

            User user1 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "first",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 60
            };

            User user2 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "second",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 1200
            };

            User user3 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "third",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 300
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user1);
                context.Users.Add(user2);
                context.Users.Add(user3);
                context.Images.Add(avatar);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                var result = await sut.GetParticipantsAsync("third", null, null);

                //Assert
                Assert.AreEqual(1, result.Count);
                Assert.AreEqual(user3.UserName, result.First().UserName);
            }
        }

        [TestMethod]
        public async Task Returns_CorrectCollection_WithSorting()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectCollection_WithSorting));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            Image avatar = new Image()
            {
                CloudinaryId = "Testing",
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png"
            };

            User user1 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "first",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 60
            };

            User user2 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "second",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 1200
            };

            User user3 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "third",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 300
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user1);
                context.Users.Add(user2);
                context.Users.Add(user3);
                context.Images.Add(avatar);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                var result = await sut.GetParticipantsAsync(null, SortByUser.Points, null);

                //Assert
                Assert.AreEqual(3, result.Count);
                Assert.AreEqual(user1.UserName, result.First().UserName);
            }
        }

        [TestMethod]
        public async Task Returns_CorrectCollection_WithSortingAndOrder()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectCollection_WithSortingAndOrder));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            Image avatar = new Image()
            {
                CloudinaryId = "Testing",
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png"
            };

            User user1 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "first",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 60
            };

            User user2 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "second",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 1200
            };

            User user3 = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "third",
                Email = "test@email.com",
                Avatar = avatar,
                Points = 300
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user1);
                context.Users.Add(user2);
                context.Users.Add(user3);
                context.Images.Add(avatar);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                var result = await sut.GetParticipantsAsync(null, SortByUser.Points, OrderBy.Desc);

                //Assert
                Assert.AreEqual(3, result.Count);
                Assert.AreEqual(user2.UserName, result.First().UserName);
            }
        }
    }
}
