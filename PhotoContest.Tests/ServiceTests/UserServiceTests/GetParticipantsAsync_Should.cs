﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class GetParticipantsAsync_Should
    {
        [TestMethod]
        public async Task Returns_CorrectCollection_Participants()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Returns_CorrectCollection_Participants));
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IOptions<AppSettings>> appSettings = new Mock<IOptions<AppSettings>>();

            User userAdmin = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "admin",
                Email = "test@email.com",
                IsAdmin = true
            };

            User userParticipant = new User()
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "participant",
                Email = "test@email.com",
            };

            using (var context = new PhotoContestDbContext(options))
            {
                context.Users.Add(userAdmin);
                context.Users.Add(userParticipant);
                await context.SaveChangesAsync();

                UserService sut = new UserService(userManager.Object, context, mapper, appSettings.Object, cloudinaryService.Object);

                //Act
                var result = await sut.GetParticipantsAsync();

                //Assert
                Assert.AreEqual(1, result.Count);
                Assert.AreEqual(userParticipant.UserName, result.First().UserName);
            }
        }
    }
}
