﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.PhotoServiceTests
{
    [TestClass]
    public class GetImagesAsync_Should
    {
        [TestMethod]
        public async Task GetsImagesCollection()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(GetsImagesCollection));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            var image1 = new Image();
            var image2 = new Image();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Images.Add(image1);
                context.Images.Add(image2);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper, cloudinaryService.Object, contestService.Object, validatorService.Object);

                //Act
                var result = await sut.GetImagesAsync();

                //Assert
                Assert.AreEqual(2, result.Count);
            }
        }
    }
}
