﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.PhotoServiceTests
{
    [TestClass]
    public class GetPhotoAsync_Should
    {
        [TestMethod]
        public async Task ReturnsCorrectPhoto()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCorrectPhoto));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            User user = new User
            {
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "testUsername",
                Email = "test@email.com"
            };
            Photo photo = new Photo
            {
                Title = "testtitle",
                Author = user
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                context.Photos.Add(photo);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper, cloudinaryService.Object, contestService.Object, validatorService.Object);

                Guid id = context.Photos.First().Id;
                Services.DTOs.PhotoDTO result = await sut.GetPhotoAsync(id);

                //Act & Assert
                Assert.AreEqual(photo.Title, result.Title);
            }

        }
    }
}
