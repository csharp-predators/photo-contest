﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.PhotoServiceTests
{
    [TestClass]
    public class AddPhotoAsync_Should
    {
        [TestMethod]
        public async Task ThrowsArgumentException_When_InvalidParticipant()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsArgumentException_When_InvalidParticipant));
            Mock<IMapper> mapper = new Mock<IMapper>();
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();
            Mock<IContestService> contestService = new Mock<IContestService>();

            PhotoDTO photoDTO = new PhotoDTO
            {
                AuthorId = Guid.NewGuid()
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                PhotoService sut = new PhotoService(context, mapper.Object, cloudinaryService.Object, contestService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.AddPhotoAsync(photoDTO, "TestTitle"));
            }
        }

        [TestMethod]
        public async Task ThrowsArgumentOutOfRangeException_When_InvalidContestPhase()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsArgumentOutOfRangeException_When_InvalidContestPhase));
            Mock<IMapper> mapper = new Mock<IMapper>();
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            PhotoDTO photoDTO = new PhotoDTO();

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase Two"
            };

            validatorService.Setup(x => x.CheckParticipantAsync(contest.Title, photoDTO.AuthorId)).ReturnsAsync(true);

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper.Object, cloudinaryService.Object, contestService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.AddPhotoAsync(photoDTO, contest.Title));
            }
        }

        [TestMethod]
        public async Task ThrowsInvalidOperationException_When_AlreadyUploadedPhoto()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsInvalidOperationException_When_AlreadyUploadedPhoto));
            Mock<IMapper> mapper = new Mock<IMapper>();
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            PhotoDTO photoDTO = new PhotoDTO();

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase One"
            };

            validatorService.Setup(x => x.CheckParticipantAsync(contest.Title, photoDTO.AuthorId)).ReturnsAsync(true);
            validatorService.Setup(x => x.CheckAlreadyUploadedPhotoAsync(contest.Title, photoDTO.AuthorId)).ReturnsAsync(true);

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper.Object, cloudinaryService.Object, contestService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.AddPhotoAsync(photoDTO, contest.Title));
            }
        }

        [TestMethod]
        public async Task AddsPhoto()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(AddsPhoto));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            CloudinaryService cloudinaryService = new CloudinaryService();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            string path = @"../../../unnamed.jpg";
            using MemoryStream stream = new MemoryStream(File.ReadAllBytes(path).ToArray());
            FormFile formFile = new FormFile(stream, 0, stream.Length, "streamFile", "fileName");

            PhotoDTO photoDTO = new PhotoDTO
            {
                Title = "testphoto",
                File = formFile
            };

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase One"
            };

            validatorService.Setup(x => x.CheckParticipantAsync(contest.Title, photoDTO.AuthorId)).ReturnsAsync(true);
            validatorService.Setup(x => x.CheckAlreadyUploadedPhotoAsync(contest.Title, photoDTO.AuthorId)).ReturnsAsync(false);

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper, cloudinaryService, contestService.Object, validatorService.Object);

                await sut.AddPhotoAsync(photoDTO, contest.Title);

                //Act & Assert
                Assert.AreEqual(context.Photos.First().Title, photoDTO.Title);
            }
        }
    }

}
