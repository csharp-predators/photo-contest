﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.PhotoServiceTests
{
    [TestClass]
    public class GetByContestAsync_Should
    {
        [TestMethod]
        public async Task ReturnsCorrectPhotosByContest()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCorrectPhotosByContest));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            var authorId = Guid.NewGuid();
            User author = new User
            {
                Id = authorId,
                FirstName = "testFirstName",
                LastName = "testLastname",
                UserName = "testUsername",
                Email = "test@email.com"
            };

            var contest = new Contest
            {
                Title = "testTitle",
                Phase = "Phase One",
                Jury = new HashSet<ContestJury>()
            };

            var photoId = Guid.NewGuid();
            Photo photo = new Photo
            {
                Id = photoId,
                Title = "testtitle",
                Author = author,
                Contests = new HashSet<ContestPhoto> { new ContestPhoto { Contest = contest, PhotoId = photoId } }
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(contest);
                context.Users.Add(author);
                context.Photos.Add(photo);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper, cloudinaryService.Object, contestService.Object, validatorService.Object);

                var result = await sut.GetByContestAsync(contest.Title);

                //Act & Assert
                Assert.AreEqual(1, result.Count);
            }

        }
    }
}
