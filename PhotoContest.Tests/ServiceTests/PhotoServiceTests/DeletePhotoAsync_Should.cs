﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.PhotoServiceTests
{
    [TestClass]
    public class DeletePhotoAsync_Should
    {
        [TestMethod]
        public async Task SetsIsDeletedTrue()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(SetsIsDeletedTrue));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Photo photo = new Photo
            {
                Title = "testphoto",
                Description = "testdescription"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Photos.Add(photo);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper, cloudinaryService.Object, contestService.Object, validatorService.Object);

                //Act
                System.Guid id = context.Photos.First().Id;
                await sut.DeletePhotoAsync(id);

                //Assert
                Assert.IsTrue(context.Photos.IgnoreQueryFilters().First().IsDeleted);
            }
        }
    }
}
