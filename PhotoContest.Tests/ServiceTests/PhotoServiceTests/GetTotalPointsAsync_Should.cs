﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.PhotoServiceTests
{
    [TestClass]
    public class GetTotalPointsAsync_Should
    {
        [TestMethod]
        public async Task ReturnsCorrectTotalPoints()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCorrectTotalPoints));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            var review1 = new Review
            {
                Score = 5,
                Comment = "testcomment"
            };

            var review2 = new Review
            {
                Score = 5,
                Comment = "testcomment"
            };

            var photoId = Guid.NewGuid();
            Photo photo = new Photo
            {
                Id = photoId,
                Title = "testtitle",
                Reviews = new HashSet<Review> { review1, review2 }
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Reviews.Add(review1);
                context.Reviews.Add(review2);
                context.Photos.Add(photo);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper, cloudinaryService.Object, contestService.Object, validatorService.Object);

                var result = await sut.GetTotalPointsAsync(photoId);

                //Act & Assert
                Assert.AreEqual(review1.Score + review2.Score, result);
            }
        }
    }
}
