﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.PhotoServiceTests
{
    [TestClass]
    public class GetLatestWinningPhotosAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCollectionOfWinningPhotos()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnCorrectCollectionOfWinningPhotos));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IContestService> contestService = new Mock<IContestService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            var photo1 = new Photo();
            var photo2 = new Photo();
            var photo3 = new Photo();
            var photo4 = new Photo();
            var photo5 = new Photo();
            var photo6 = new Photo();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Photos.Add(photo1);
                context.Photos.Add(photo2);
                context.Photos.Add(photo3);
                context.Photos.Add(photo4);
                context.Photos.Add(photo5);
                context.Photos.Add(photo6);
                await context.SaveChangesAsync();

                PhotoService sut = new PhotoService(context, mapper, cloudinaryService.Object, contestService.Object, validatorService.Object);

                //Act
                var result = await sut.GetLatestWinningPhotosAsync();

                //Assert
                Assert.AreEqual(6, result.Count);
            }
        }
    }
}
