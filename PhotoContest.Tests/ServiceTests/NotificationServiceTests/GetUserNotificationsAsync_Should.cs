﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.NotificationServiceTests
{
    [TestClass]
    public class GetUserNotificationsAsync_Should
    {
        [TestMethod]
        public async Task ReturnsNotReadedNotificationByUser()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsNotReadedNotificationByUser));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            var contest = new Contest { Title = "testTitle" };

            var userId = Guid.NewGuid();
            var user = new User
            {
                Id = userId,
                UserName = "participant",
                FirstName = "participantFN",
                LastName = "participantLN",
                Email = "participant@email.com"
            };

            var notification1 = new Notification { IsReaded = true, User = user, Contest = contest };
            var notification2 = new Notification { IsReaded = false, User = user, Contest = contest };
            var notification3 = new Notification { IsReaded = false, User = user, Contest = contest };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                NotificationService sut = new NotificationService(context, mapper);

                context.Users.Add(user);
                context.Contests.Add(contest);
                context.Notifications.Add(notification1);
                context.Notifications.Add(notification2);
                context.Notifications.Add(notification3);
                await context.SaveChangesAsync();

                //Act
                var result = await sut.GetUserNotificationsAsync(userId);

                //Assert
                Assert.IsTrue(result.Count == 2);
            }
        }
    }
}
