﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.NotificationServiceTests
{
    [TestClass]
    public class MarkNoticationAsReadedAsync_Should
    {
        [TestMethod]
        public async Task MarksNoticationAsReaded()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(MarksNoticationAsReaded));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            var notificationId = Guid.NewGuid();
            var notification = new Notification { Id = notificationId };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                NotificationService sut = new NotificationService(context, mapper);

                context.Notifications.Add(notification);
                await context.SaveChangesAsync();

                //Act
                await sut.MarkNoticationAsReadedAsync(notificationId);

                //Assert
                Assert.IsTrue(context.Notifications.First().IsReaded);
            }
        }
    }
}
