﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Services;
using PhotoContest.Services.DTOs;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CategoryServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task Returns_CorrectCategory()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_CorrectCategory));
            var mapper = new Mapper(Utils.GetConfigurationMapper());

            var categoryDTO = new CategoryDTO
            {
                Name = "Test"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                CategoryService sut = new CategoryService(context, mapper);
                await sut.CreateAsync(categoryDTO);

                //Act
                var result = await sut.GetAsync("Test");

                //Assert
                Assert.AreEqual(categoryDTO.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_CategoryNotExists()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowException_When_CategoryNotExists));
            var mapper = new Mock<IMapper>();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                CategoryService sut = new CategoryService(context, mapper.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync("invalidCategoryName"));
            }
        }
    }
}
