﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Services;
using PhotoContest.Services.DTOs;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CategoryServiceTests
{
    [TestClass]
    public class GetCategoryAsync_Should
    {
        [TestMethod]
        public async Task Returns_CorrectCategory()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_CorrectCategory));
            var mapper = new Mapper(Utils.GetConfigurationMapper());

            var categoryDTO = new CategoryDTO
            {
                Name = "Test"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                CategoryService sut = new CategoryService(context, mapper);
                await sut.CreateAsync(categoryDTO);

                //Act
                var result = await sut.GetCategoryAsync("Test");

                //Assert
                Assert.AreEqual(categoryDTO.Name, result.Name);
            }
        }
    }
}
