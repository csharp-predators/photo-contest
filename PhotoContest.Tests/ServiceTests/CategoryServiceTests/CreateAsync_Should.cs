﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Services;
using PhotoContest.Services.DTOs;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CategoryServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task CreatesNewCategory()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(CreatesNewCategory));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            var categoryDTO = new CategoryDTO
            {
                Name = "TestName"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                CategoryService sut = new CategoryService(context, mapper);

                //Act
                await sut.CreateAsync(categoryDTO);

                //Assert
                Assert.AreEqual(context.Categories.First().Name, categoryDTO.Name);
            }
        }
    }
}
