﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ValidatorServiceTests
{
    [TestClass]
    public class CheckAlreadyUploadedPhotoAsync_Should
    {
        [TestMethod]
        public async Task Returns_True_IfUserAlreadyUploaded()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_True_IfUserAlreadyUploaded));

            User author = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Photo photo = new Photo
            {
                Title = "testtitle",
                Author = author
            };

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase Two",
            };

            ContestPhoto contestPhoto = new ContestPhoto()
            {
                Photo = photo,
                Contest = contest
            };

            contest.Photos.Add(contestPhoto);

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(author);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckAlreadyUploadedPhotoAsync("testcontest", author.Id);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task Returns_False_IfUserHasntUploadedYet()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_False_IfUserHasntUploadedYet));

            User author = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase Two",
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(author);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckAlreadyUploadedPhotoAsync("testcontest", author.Id);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
