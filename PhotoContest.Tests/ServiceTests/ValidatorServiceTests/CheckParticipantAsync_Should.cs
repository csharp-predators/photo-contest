﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ValidatorServiceTests
{
    [TestClass]
    public class CheckParticipantAsync_Should
    {
        [TestMethod]
        public async Task Throw_Exception_WhenContestTitleNull()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Throw_Exception_WhenContestTitleNull));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CheckParticipantAsync("TestWrongTitle", user.Id));
            }
        }

        [TestMethod]
        public async Task Returns_True_WhenUserParticipant_InContest()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_True_WhenUserParticipant_InContest));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase Two"
            };

            ContestParticipant contestJury = new ContestParticipant()
            {
                Participant = user,
                Contest = contest
            };
            contest.Participants.Add(contestJury);

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckParticipantAsync(contest.Title, user.Id);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task Returns_False_WhenUserNotParticipant_InContest()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_False_WhenUserNotParticipant_InContest));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase Two"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckParticipantAsync(contest.Title, user.Id);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
