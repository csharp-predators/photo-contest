﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ValidatorServiceTests
{
    [TestClass]
    public class CheckAdminAsync_Should
    {
        [TestMethod]
        public async Task Returns_True_IfUserIsAdmin()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_True_IfUserIsAdmin));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com",
                IsAdmin = true
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckAdminAsync(user.Id);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task Returns_False_IfUserIsNotAdmin()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_False_IfUserIsNotAdmin));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckAdminAsync(user.Id);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
