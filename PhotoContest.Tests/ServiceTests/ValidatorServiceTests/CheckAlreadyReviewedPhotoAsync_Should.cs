﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ValidatorServiceTests
{
    [TestClass]
    public class CheckAlreadyReviewedPhotoAsync_Should
    {
        [TestMethod]
        public async Task Returns_True_When_Reviewed()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_True_When_Reviewed));

            User author = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            var review = new Review
            {
                Score = 5,
                Comment = "testcomment",
                Author = author
            };

            var photoId = Guid.NewGuid();
            Photo photo = new Photo
            {
                Id = photoId,
                Title = "testtitle",
                Reviews = new HashSet<Review> { review }
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(author);
                context.Reviews.Add(review);
                context.Photos.Add(photo);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = sut.CheckAlreadyReviewedPhotoAsync(photoId, author.Id);
                
                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task Returns_False_When_NotReviewed()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_True_When_Reviewed));

            User author = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            var photoId = Guid.NewGuid();
            Photo photo = new Photo
            {
                Id = photoId,
                Title = "testtitle",
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(author);
                context.Photos.Add(photo);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = sut.CheckAlreadyReviewedPhotoAsync(photoId, author.Id);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
