﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ValidatorServiceTests
{
    [TestClass]
    public class ValidGuidString_Should
    {
        [TestMethod]
        public void Return_True_IfValidGuid()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Return_True_IfValidGuid));
            string guid = Guid.NewGuid().ToString();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = sut.ValidGuidString(guid);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public void Return_False_IfNotValidGuid()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Return_False_IfNotValidGuid));
            string guid = "TestingWrongGuid";

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = sut.ValidGuidString(guid);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
