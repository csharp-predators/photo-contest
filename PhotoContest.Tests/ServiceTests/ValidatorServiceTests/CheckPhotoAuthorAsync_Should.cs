﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ValidatorServiceTests
{
    [TestClass]
    public class CheckPhotoAuthorAsync_Should
    {
        [TestMethod]
        public async Task Throws_Exception_WhenPhotoNull()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Throws_Exception_WhenPhotoNull));

            Guid userId = Guid.NewGuid();

            Guid photoId = Guid.NewGuid();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ValidatorService sut = new ValidatorService(context);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CheckPhotoAuthorAsync(photoId, userId));
            }
        }

        [TestMethod]
        public async Task Returns_True_IfPhotoAuthorCorrect()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_True_IfPhotoAuthorCorrect));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Photo photo = new Photo
            {
                Title = "testtitle",
                Author = user,
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                context.Photos.Add(photo);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckPhotoAuthorAsync(photo.Id, user.Id);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task Returns_False_IfPhotoAuthorNotCorrect()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Returns_False_IfPhotoAuthorNotCorrect));

            Guid userId = Guid.NewGuid();

            Photo photo = new Photo
            {
                Title = "testtitle",
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Photos.Add(photo);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckPhotoAuthorAsync(photo.Id, userId);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
