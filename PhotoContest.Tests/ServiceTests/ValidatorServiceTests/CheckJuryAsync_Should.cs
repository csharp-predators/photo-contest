﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ValidatorServiceTests
{
    [TestClass]
    public class CheckJuryAsync_Should
    {
        [TestMethod]
        public async Task ThrowsException_When_Contest_Null()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsException_When_Contest_Null));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CheckJuryAsync("TestWrongTitle", user.Id));
            }
        }

        [TestMethod]
        public async Task Return_True_WhenUserIsJury()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Return_True_WhenUserIsJury));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase Two"
            };

            ContestJury contestJury = new ContestJury()
            {
                Jury = user,
                Contest = contest
            };
            contest.Jury.Add(contestJury);

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckJuryAsync(contest.Title, user.Id);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task Return_False_WhenUserIsNotJury()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Return_False_WhenUserIsNotJury));

            User user = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Contest contest = new Contest
            {
                Title = "testcontest",
                Phase = "Phase Two"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(user);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ValidatorService sut = new ValidatorService(context);

                //Act
                bool result = await sut.CheckJuryAsync(contest.Title, user.Id);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
