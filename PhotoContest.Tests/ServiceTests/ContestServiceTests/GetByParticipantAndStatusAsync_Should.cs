﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class GetByParticipantAndStatusAsync_Should
    {
        [TestMethod]
        public async Task ReturnsOrderedCollectionWithStatusCurrent()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsOrderedCollectionWithStatusCurrent));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Category category = new Category { Name = "testCategory" };
            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };
            User participant = new User
            {
                UserName = "testUsername1",
                FirstName = "test1FirstName",
                LastName = "test1LastName",
                Email = "test1@email.com"
            };
            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            Guid contest1Id = Guid.NewGuid();
            Contest contest1 = new Contest
            {
                Title = "testTitle1",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover,
            };
            contest1.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest1Id });

            Guid contest2Id = Guid.NewGuid();
            Contest contest2 = new Contest
            {
                Title = "testTitle2",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_TWO_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest2.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest2Id });

            Guid contest3Id = Guid.NewGuid();
            Contest contest3 = new Contest
            {
                Title = "testTitle3",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_FINISHED_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest3.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest3Id });

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(participant);
                context.Users.Add(creator);
                context.Categories.Add(category);
                context.Images.Add(cover);
                context.Contests.Add(contest1);
                context.Contests.Add(contest2);
                context.Contests.Add(contest3);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                ICollection<Services.DTOs.ContestDTO> result = await sut.GetByParticipantAndStatusAsync(participant.Id, "current", null, null, null);

                //Assert
                Assert.IsTrue(result.All(x => x.Phase == Constants.Contest.PHASE_ONE_STRING || x.Phase == Constants.Contest.PHASE_TWO_STRING));
            }
        }

        [TestMethod]
        public async Task ReturnsOrderedCollectionWithStatusFinished()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsOrderedCollectionWithStatusFinished));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Category category = new Category { Name = "testCategory" };
            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };
            User participant = new User
            {
                UserName = "testUsername1",
                FirstName = "test1FirstName",
                LastName = "test1LastName",
                Email = "test1@email.com"
            };
            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            Guid contest1Id = Guid.NewGuid();
            Contest contest1 = new Contest
            {
                Title = "testTitle1",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover,
            };
            contest1.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest1Id });

            Guid contest2Id = Guid.NewGuid();
            Contest contest2 = new Contest
            {
                Title = "testTitle2",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_TWO_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest2.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest2Id });

            Guid contest3Id = Guid.NewGuid();
            Contest contest3 = new Contest
            {
                Title = "testTitle3",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_FINISHED_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest3.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest3Id });

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(participant);
                context.Users.Add(creator);
                context.Categories.Add(category);
                context.Images.Add(cover);
                context.Contests.Add(contest1);
                context.Contests.Add(contest2);
                context.Contests.Add(contest3);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                ICollection<Services.DTOs.ContestDTO> result = await sut.GetByParticipantAndStatusAsync(participant.Id, "finished", null, null, null);

                //Assert
                Assert.IsTrue(result.All(x => x.Phase == Constants.Contest.PHASE_FINISHED_STRING));
            }
        }

        [TestMethod]
        public async Task Ordered_ThrowsArgumentException_When_InvalidPhase()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Ordered_ThrowsArgumentException_When_InvalidPhase));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetByParticipantAndStatusAsync(Guid.NewGuid(), "invalidStatus", null, null, null));
            }
        }


        [TestMethod]
        public async Task ReturnsCollectionWithStatusCurrent()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCollectionWithStatusCurrent));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Category category = new Category { Name = "testCategory" };
            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };
            User participant = new User
            {
                UserName = "testUsername1",
                FirstName = "test1FirstName",
                LastName = "test1LastName",
                Email = "test1@email.com"
            };
            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            Guid contest1Id = Guid.NewGuid();
            Contest contest1 = new Contest
            {
                Title = "testTitle1",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover,
            };
            contest1.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest1Id });

            Guid contest2Id = Guid.NewGuid();
            Contest contest2 = new Contest
            {
                Title = "testTitle2",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_TWO_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest2.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest2Id });

            Guid contest3Id = Guid.NewGuid();
            Contest contest3 = new Contest
            {
                Title = "testTitle3",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_FINISHED_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest3.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest3Id });

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(participant);
                context.Users.Add(creator);
                context.Categories.Add(category);
                context.Images.Add(cover);
                context.Contests.Add(contest1);
                context.Contests.Add(contest2);
                context.Contests.Add(contest3);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                ICollection<Services.DTOs.ContestDTO> result = await sut.GetByParticipantAndStatusAsync(participant.Id, "current");

                //Assert
                Assert.IsTrue(result.All(x => x.Phase == Constants.Contest.PHASE_ONE_STRING || x.Phase == Constants.Contest.PHASE_TWO_STRING));
            }
        }

        [TestMethod]
        public async Task ReturnsCollectionWithStatusFinished()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCollectionWithStatusFinished));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Category category = new Category { Name = "testCategory" };
            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };
            User participant = new User
            {
                UserName = "testUsername1",
                FirstName = "test1FirstName",
                LastName = "test1LastName",
                Email = "test1@email.com"
            };
            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            Guid contest1Id = Guid.NewGuid();
            Contest contest1 = new Contest
            {
                Title = "testTitle1",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover,
            };
            contest1.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest1Id });

            Guid contest2Id = Guid.NewGuid();
            Contest contest2 = new Contest
            {
                Title = "testTitle2",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_TWO_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest2.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest2Id });

            Guid contest3Id = Guid.NewGuid();
            Contest contest3 = new Contest
            {
                Title = "testTitle3",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_FINISHED_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest3.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contest3Id });

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(participant);
                context.Users.Add(creator);
                context.Categories.Add(category);
                context.Images.Add(cover);
                context.Contests.Add(contest1);
                context.Contests.Add(contest2);
                context.Contests.Add(contest3);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                ICollection<Services.DTOs.ContestDTO> result = await sut.GetByParticipantAndStatusAsync(participant.Id, "finished");

                //Assert
                Assert.IsTrue(result.All(x => x.Phase == Constants.Contest.PHASE_FINISHED_STRING));
            }
        }

        [TestMethod]
        public async Task ThrowsArgumentException_When_InvalidPhase()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsArgumentException_When_InvalidPhase));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetByParticipantAndStatusAsync(Guid.NewGuid(), "invalidStatus"));
            }
        }
    }
}
