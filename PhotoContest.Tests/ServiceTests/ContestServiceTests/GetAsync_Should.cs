﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task ThrowsArgumentException_When_InvalidContest()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsArgumentException_When_InvalidContest));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync("testTitle"));
            }
        }

        [TestMethod]
        public async Task ReturnsContest()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsContest));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Category category = new Category { Name = "testCategory" };
            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };
            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            string contestTitle = "testTitle";
            Contest contest = new Contest 
            { 
                Title = contestTitle,
                Category = category,
                Creator = creator,
                Cover = cover
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Categories.Add(category);
                context.Images.Add(cover);
                context.Users.Add(creator);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                var result = await sut.GetAsync(contestTitle);

                //Act & Assert
                Assert.AreEqual(contestTitle, result.Title);
            }
        }
    }
}
