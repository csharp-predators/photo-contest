﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class AddParticipantAsync_Should
    {
        [TestMethod]
        public async Task AddsParticipantToContest()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(AddsParticipantToContest));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();
            string contestTitle = "testtitle";
            Guid userId = Guid.NewGuid();
            validatorService.Setup(x => x.CheckParticipantAsync(contestTitle, userId)).ReturnsAsync(false);

            Contest contest = new Contest
            {
                Title = contestTitle,
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING
            };

            User user = new User
            {
                Id = userId,
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com",
                ContestsAsParticipant = new HashSet<ContestParticipant>()
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(contest);
                context.Users.Add(user);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                await sut.AddParticipantAsync(userId, contestTitle);

                //Assert
                Assert.IsTrue(context.Users.First().Points == 1);
            }
        }

        [TestMethod]
        public async Task ThrowsInvalidOperationException_When_AlreadyJoined()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsInvalidOperationException_When_AlreadyJoined));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();
            string contestTitle = "testtitle";
            Guid userId = Guid.NewGuid();
            validatorService.Setup(x => x.CheckParticipantAsync(contestTitle, userId)).ReturnsAsync(true);

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(new Contest { Title = contestTitle, Participants = new HashSet<ContestParticipant>(), Phase = Constants.Contest.PHASE_ONE_STRING });
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(() => sut.AddParticipantAsync(userId, contestTitle));
            }
        }

        [TestMethod]
        public async Task ThrowsArgumentException_When_InvalidContest()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsArgumentException_When_InvalidContest));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.AddParticipantAsync(Guid.NewGuid(), "testTitle"));
            }
        }
    }
}
