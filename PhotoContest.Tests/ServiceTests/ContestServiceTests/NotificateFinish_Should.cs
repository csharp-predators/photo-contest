﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class NotificateFinish_Should
    {
        [TestMethod]
        public async Task NotificatesAdminsParticipantsJury()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(NotificatesAdminsParticipantsJury));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            CloudinaryService cloudinaryService = new CloudinaryService();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            var admin = new User
            {
                UserName = "admin",
                FirstName = "adminFN",
                LastName = "adminLN",
                Email = "admin@email.com",
                IsAdmin = true
            };

            var jury = new User
            {
                UserName = "jury",
                FirstName = "juryFN",
                LastName = "juryLN",
                Email = "jury@email.com"
            };

            var participant = new User
            {
                UserName = "participant",
                FirstName = "participantFN",
                LastName = "participantLN",
                Email = "participant@email.com"
            };

            var contestId = Guid.NewGuid();
            var contest = new Contest
            {
                Id = contestId,
                Title = "testTitle"
            };
            contest.Participants.Add(new ContestParticipant { Participant = participant, ContestId = contestId });
            contest.Jury.Add(new ContestJury { Jury = jury, ContestId = contestId });

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService, validatorService.Object);

                context.Users.Add(admin);
                context.Users.Add(jury);
                context.Users.Add(participant);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                //Act
                sut.NotificateFinish(context.Contests.First());
                await context.SaveChangesAsync();

                //Assert
                Assert.IsTrue(context.Notifications.Count() == 2);
            }
        }
    }
}
