﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task AddsContestWithDefaultCover()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(AddsContestWithDefaultCover));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var cloudinaryService = new CloudinaryService();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            string path = @"../../../unnamed.jpg";
            using MemoryStream stream = new MemoryStream(File.ReadAllBytes(path).ToArray());
            FormFile formFile = new FormFile(stream, 0, stream.Length, "streamFile", "fileName");

            ContestDTO contestDTO = new ContestDTO
            {
                Title = "testTitle",
                ParticipantsUsernames = new List<string>(),
                JuryUsernames = new List<string>()
            };

            Image cover1 = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622752891/covers/x6ptjebvf5jwwuegvejf.png",
                CloudinaryId = "cloudinaryTestId1"
            };

            Image cover2 = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622752891/covers/x6ptjebvf5jwwuegvejf.png",
                CloudinaryId = "cloudinaryTestId2"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Images.Add(cover1);
                context.Images.Add(cover2);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService, validatorService.Object);

                //Act
                await sut.CreateAsync(contestDTO);

                //Assert
                Assert.AreEqual(contestDTO.Title, context.Contests.First().Title);
            }
        }

        [TestMethod]
        public async Task AddsContestWithCoverFromFile()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(AddsContestWithCoverFromFile));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var cloudinaryService = new CloudinaryService();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            string path = @"../../../unnamed.jpg";
            using MemoryStream stream = new MemoryStream(File.ReadAllBytes(path).ToArray());
            FormFile formFile = new FormFile(stream, 0, stream.Length, "streamFile", "fileName");

            ContestDTO contestDTO = new ContestDTO
            {
                Title = "testTitle",
                CoverFile = formFile,
                ParticipantsUsernames = new List<string>(),
                JuryUsernames = new List<string>()
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService, validatorService.Object);

                //Act
                await sut.CreateAsync(contestDTO);

                //Assert
                Assert.AreEqual(contestDTO.Title, context.Contests.First().Title);
            }
        }

        [TestMethod]
        public async Task AddsContestWithURLCover()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(AddsContestWithURLCover));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var cloudinaryService = new CloudinaryService();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            ContestDTO contestDTO = new ContestDTO
            {
                Title = "testTitle",
                CoverURL = "https://acmilan-bg.com/snimki-novini/acmilanbglogonews.png",
                ParticipantsUsernames = new List<string>(),
                JuryUsernames = new List<string>()
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService, validatorService.Object);

                //Act
                await sut.CreateAsync(contestDTO);

                //Assert
                Assert.AreEqual(contestDTO.Title, context.Contests.First().Title);
            }
        }

        [TestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task AddsContestWithDBCover(bool masterCanBeJury)
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(AddsContestWithDBCover) + masterCanBeJury.ToString());
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            var admin = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com",
                IsAdmin = true
            };

            var jury = new User
            {
                UserName = "test2Username",
                FirstName = "test2FirstName",
                LastName = "test2LastName",
                Email = "test2@email.com",
                Points = 1200
            };

            var participant = new User
            {
                UserName = "test1Username",
                FirstName = "test1FirstName",
                LastName = "test1LastName",
                Email = "test1@email.com",
            };

            var coverId = Guid.NewGuid();
            Image cover = new Image
            {
                Id = coverId,
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            ContestDTO contestDTO = new ContestDTO
            {
                Title = "testTitle",
                IsInvitational = true,
                CoverId = coverId,
                MasterCanBeJury = masterCanBeJury,
                ParticipantsUsernames = new List<string> { participant.UserName, null },
                JuryUsernames = new List<string> { jury.UserName, null}
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                context.Users.Add(jury);
                context.Users.Add(participant);
                context.Users.Add(admin);
                context.Images.Add(cover);
                await context.SaveChangesAsync();

                //Act
                await sut.CreateAsync(contestDTO);

                //Assert
                Assert.AreEqual(contestDTO.Title, context.Contests.First().Title);
            }
        }

        [TestMethod]
        public async Task ThrowsArgumentException_When_TitleExists()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsArgumentException_When_TitleExists));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Contest contest = new Contest
            {
                Title = "testTitle"
            };

            ContestDTO contest2 = new ContestDTO
            {
                Title = "testTitle"
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(contest2));
            }
        }
    }
}
