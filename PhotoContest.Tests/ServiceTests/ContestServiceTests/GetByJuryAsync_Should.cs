﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class GetByJuryAsync_Should
    {
        [TestMethod]
        public async Task ReturnsCorrectCollection()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCorrectCollection));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Category category = new Category { Name = "testCategory" };

            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Guid juryId = Guid.NewGuid();
            User jury = new User
            {
                Id = juryId,
                UserName = "test1Username",
                FirstName = "test1FirstName",
                LastName = "test1LastName",
                Email = "test1@email.com"
            };

            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            Guid contestId = Guid.NewGuid();
            Contest contest = new Contest
            {
                Id = contestId,
                Title = "testTitle",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };
            contest.Jury.Add(new ContestJury { Jury = jury, ContestId = contestId });

            Contest contest2 = new Contest
            {
                Title = "test1Title",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                context.Categories.Add(category);
                context.Users.Add(creator);
                context.Users.Add(jury);
                context.Images.Add(cover);
                context.Contests.Add(contest);
                context.Contests.Add(contest2);
                await context.SaveChangesAsync();

                //Act
                ICollection<Services.DTOs.ContestDTO> result = await sut.GetByJuryAsync(juryId, null, null, null);

                //Assert
                Assert.IsTrue(result.Count == 1);
            }
        }
    }
}
