﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class GetByPhaseAsync_Should
    {
        [TestMethod]
        [DataRow("one")]
        [DataRow("two")]
        [DataRow("finished")]
        public async Task ReturnsOrderedCollectionWithCorrectPhase(string phase)
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsOrderedCollectionWithCorrectPhase));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Contest[] arr = new Contest[]
            {
                new Contest { Title = "testTitle1", Creator = creator, Phase = Constants.Contest.PHASE_ONE_STRING },
                new Contest { Title = "testTitle2", Creator = creator, Phase = Constants.Contest.PHASE_ONE_STRING },
                new Contest { Title = "testTitle3", Creator = creator, Phase = Constants.Contest.PHASE_TWO_STRING },
                new Contest { Title = "testTitle4", Creator = creator, Phase = Constants.Contest.PHASE_FINISHED_STRING },
                new Contest { Title = "testTitle5", Creator = creator, Phase = Constants.Contest.PHASE_FINISHED_STRING },
                new Contest { Title = "testTitle6", Creator = creator, Phase = Constants.Contest.PHASE_FINISHED_STRING }
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(creator);
                context.Contests.AddRange(arr);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                ICollection<Services.DTOs.ContestDTO> result = await sut.GetByPhaseAsync(phase, null, null, null);

                //Assert
                Assert.IsTrue(result.All(x => x.Phase == phase));
            }
        }

        [TestMethod]
        [DataRow("one")]
        [DataRow("two")]
        [DataRow("finished")]
        public async Task ReturnsCollectionWithCorrectPhase(string phase)
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCollectionWithCorrectPhase));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Contest[] arr = new Contest[]
            {
                new Contest { Title = "testTitle1", Creator = creator, Phase = Constants.Contest.PHASE_ONE_STRING },
                new Contest { Title = "testTitle2", Creator = creator, Phase = Constants.Contest.PHASE_ONE_STRING },
                new Contest { Title = "testTitle3", Creator = creator, Phase = Constants.Contest.PHASE_TWO_STRING },
                new Contest { Title = "testTitle4", Creator = creator, Phase = Constants.Contest.PHASE_FINISHED_STRING },
                new Contest { Title = "testTitle5", Creator = creator, Phase = Constants.Contest.PHASE_FINISHED_STRING },
                new Contest { Title = "testTitle6", Creator = creator, Phase = Constants.Contest.PHASE_FINISHED_STRING }
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(creator);
                context.Contests.AddRange(arr);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                ICollection<Services.DTOs.ContestDTO> result = await sut.GetByPhaseAsync(phase);

                //Assert
                Assert.IsTrue(result.All(x => x.Phase == phase));
            }
        }

        [TestMethod]
        public async Task Throws_Exception_WhenInvalidPhase()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(Throws_Exception_WhenInvalidPhase));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetByPhaseAsync("wrongPhase"));
            }
        }
    }
}
