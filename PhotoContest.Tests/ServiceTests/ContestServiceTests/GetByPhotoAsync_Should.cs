﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class GetByPhotoAsync_Should
    {
        [TestMethod]
        public async Task ReturnsContest()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsContest));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();

            Guid photoId = Guid.NewGuid();
            Photo photo = new Photo { Id = photoId, Title = "testTitle" };
            Guid contestId = Guid.NewGuid();
            Contest contest = new Contest { Id = contestId, Title = "testTitle" };
            contest.Photos.Add(new ContestPhoto { PhotoId = photoId, ContestId = contestId });

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Photos.Add(photo);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                Services.DTOs.ContestDTO result = await sut.GetByPhotoAsync(photoId);

                //Assert
                Assert.AreEqual(contestId, result.Id);
            }
        }
    }
}
