﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ContestServiceTests
{
    [TestClass]
    public class GetAllAsync_Should
    {
        [TestMethod]
        public async Task ReturnsCollection()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCollection));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();
            string contestTitle = "testtitle";
            Guid userId = Guid.NewGuid();
            validatorService.Setup(x => x.CheckParticipantAsync(contestTitle, userId)).ReturnsAsync(false);

            Category category = new Category { Name = "testCategory" };
            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };
            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            Contest contest1 = new Contest
            {
                Title = contestTitle,
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };

            Contest contest2 = new Contest
            {
                Title = contestTitle,
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Categories.Add(category);
                context.Users.Add(creator);
                context.Images.Add(cover);
                context.Contests.Add(contest1);
                context.Contests.Add(contest2);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                ICollection<Services.DTOs.ContestDTO> result = await sut.GetAllAsync(null, null, null);

                //Assert
                Assert.IsTrue(result.Count == 2);
            }
        }

        [TestMethod]
        public async Task ReturnsCollectionSortedByTitleDescending()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCollectionSortedByTitleDescending));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();
            string contestTitle = "testtitle";
            Guid userId = Guid.NewGuid();
            validatorService.Setup(x => x.CheckParticipantAsync(contestTitle, userId)).ReturnsAsync(false);

            Category category = new Category { Name = "testCategory" };
            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };
            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            Contest contest1 = new Contest
            {
                Title = contestTitle,
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };

            Contest contest2 = new Contest
            {
                Title = "aaaaaaaaa",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Categories.Add(category);
                context.Users.Add(creator);
                context.Images.Add(cover);
                context.Contests.Add(contest1);
                context.Contests.Add(contest2);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                var result = await sut.GetAllAsync(null, SortBy.Title, OrderBy.Desc);

                //Assert
                Assert.IsTrue(result.Count == 2);
                Assert.IsTrue(result.First().Title == contest1.Title);
            }
        }

        [TestMethod]
        public async Task ReturnsCollectionSortedByTitleAscending()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCollectionSortedByTitleAscending));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());
            Mock<ICloudinaryService> cloudinaryService = new Mock<ICloudinaryService>();
            Mock<IValidatorService> validatorService = new Mock<IValidatorService>();
            string contestTitle = "testtitle";
            Guid userId = Guid.NewGuid();
            validatorService.Setup(x => x.CheckParticipantAsync(contestTitle, userId)).ReturnsAsync(false);

            Category category = new Category { Name = "testCategory" };
            Category category2 = new Category { Name = "aaaaaaaaaaa" };
            User creator = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };
            Image cover = new Image
            {
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png",
                CloudinaryId = "testCloudinaryId"
            };

            Contest contest1 = new Contest
            {
                Title = contestTitle,
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category,
                Creator = creator,
                Cover = cover
            };

            Contest contest2 = new Contest
            {
                Title = "aaaaaaaaa",
                Participants = new HashSet<ContestParticipant>(),
                Phase = Constants.Contest.PHASE_ONE_STRING,
                Category = category2,
                Creator = creator,
                Cover = cover
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Categories.Add(category);
                context.Users.Add(creator);
                context.Images.Add(cover);
                context.Contests.Add(contest1);
                context.Contests.Add(contest2);
                await context.SaveChangesAsync();

                ContestService sut = new ContestService(context, mapper, cloudinaryService.Object, validatorService.Object);

                //Act
                var result = await sut.GetAllAsync(null, SortBy.Category, OrderBy.Asc);

                //Assert
                Assert.IsTrue(result.Count == 2);
                Assert.IsTrue(result.Last().Title == contest1.Title);
            }
        }
    }
}
