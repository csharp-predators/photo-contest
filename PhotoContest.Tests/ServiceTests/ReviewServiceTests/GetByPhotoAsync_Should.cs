﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ReviewServiceTests
{
    [TestClass]
    public class GetByPhotoAsync_Should
    {
        [TestMethod]
        public async Task ReturnsCorrectCollection()
        {
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ReturnsCorrectCollection));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            User author = new User
            {
                UserName = "testUsername",
                FirstName = "testFirstName",
                LastName = "testLastName",
                Email = "test@email.com"
            };

            Guid photoId = Guid.NewGuid();
            Photo photo = new Photo
            {
                Id = photoId,
                Title = "testTitle",
                Description = "testDescription",
                Reviews = new HashSet<Review> { new Review { Score = 5, Comment = "testComment", Author = author } }
            };

            Guid contestId = Guid.NewGuid();
            Contest contest = new Contest
            {
                Id = contestId,
                Title = "testTitle",
                Phase = "Phase Two"
            };
            contest.Photos = new HashSet<ContestPhoto> { new ContestPhoto { PhotoId = photoId, ContestId = contestId } };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Users.Add(author);
                context.Photos.Add(photo);
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ReviewService sut = new ReviewService(context, mapper);

                //Act
                ICollection<Services.DTOs.ReviewDTO> result = await sut.GetByPhotoAsync(photoId, contest.Title);

                //Assert
                Assert.IsTrue(result.Any(x => x.Score == 5));
            }

        }

        [TestMethod]
        public async Task ThrowsFileNotFoundException()
        {
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsFileNotFoundException));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            Guid contestId = Guid.NewGuid();
            Contest contest = new Contest
            {
                Id = contestId,
                Title = "testTitle",
                Phase = "Phase Two",
                Photos = new HashSet<ContestPhoto>()
            };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                ReviewService sut = new ReviewService(context, mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<FileNotFoundException>(() => sut.GetByPhotoAsync(Guid.NewGuid(), contest.Title));
            }
        }
    }
}
