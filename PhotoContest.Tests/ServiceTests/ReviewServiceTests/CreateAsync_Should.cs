﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services;
using PhotoContest.Services.DTOs;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.ReviewServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task AddsReview()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(AddsReview));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            var contestId = Guid.NewGuid();
            var contest = new Contest { Id = contestId, Title = "testTitle", Phase = "Phase Two" };
            var reviewDTO = new ReviewDTO { Score = 5, Comment = "testComment", ContestId = contestId };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                var sut = new ReviewService(context, mapper);

                //Act
                await sut.CreateAsync(reviewDTO);
                
                //Assert
                Assert.AreEqual(reviewDTO.Comment, context.Reviews.First().Comment);
            }
        }

        [TestMethod]
        public async Task ThrowsArgumentOutOfRangeException_When_ContestPhaseInvalid()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(ThrowsArgumentOutOfRangeException_When_ContestPhaseInvalid));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            var contestId = Guid.NewGuid();
            var contest = new Contest { Id = contestId, Title = "testTitle", Phase = "testPhase" };
            var reviewDTO = new ReviewDTO { Score = 5, Comment = "testComment", ContestId = contestId };

            using (PhotoContestDbContext context = new PhotoContestDbContext(options))
            {
                context.Contests.Add(contest);
                await context.SaveChangesAsync();

                var sut = new ReviewService(context, mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.CreateAsync(reviewDTO));
            }
        }
    }
}
