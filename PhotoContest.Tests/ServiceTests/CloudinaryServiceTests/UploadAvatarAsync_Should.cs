﻿using AutoMapper;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Services;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CloudinaryServiceTests
{
    [TestClass]
    public class UploadAvatarAsync_Should
    {
        [TestMethod]
        public async Task UploadsAvatar()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(UploadsAvatar));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            string path = @"../../../unnamed.jpg";
            using MemoryStream stream = new MemoryStream(File.ReadAllBytes(path).ToArray());
            FormFile formFile = new FormFile(stream, 0, stream.Length, "streamFile", "fileName");

            CloudinaryService sut = new CloudinaryService();

            //Act
            ImageUploadResult result = await sut.UploadAvatarAsync(formFile);

            //Assert
            Assert.IsTrue(result.Url != null);
        }
    }
}

