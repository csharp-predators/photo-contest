﻿using AutoMapper;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Services;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CloudinaryServiceTests
{
    [TestClass]
    public class UploadCoverAsync_Should
    {
        [TestMethod]
        public async Task UploadsCoverFromURL()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(UploadsCoverFromURL));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            CloudinaryService sut = new CloudinaryService();

            //Act
            ImageUploadResult result = await sut.UploadCoverAsync("https://acmilan-bg.com/snimki-novini/acmilanbglogonews.png");

            //Assert
            Assert.IsTrue(result.Url != null);
        }

        [TestMethod]
        public async Task UploadsCoverFromFile()
        {
            //Arrange
            DbContextOptions<PhotoContestDbContext> options = Utils.GetOptions(nameof(UploadsCoverFromFile));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            string path = @"../../../unnamed.jpg";
            using MemoryStream stream = new MemoryStream(File.ReadAllBytes(path).ToArray());
            FormFile formFile = new FormFile(stream, 0, stream.Length, "streamFile", "fileName");

            CloudinaryService sut = new CloudinaryService();

            //Act
            ImageUploadResult result = await sut.UploadCoverAsync(formFile);

            //Assert
            Assert.IsTrue(result.Url != null);
        }
    }
}
