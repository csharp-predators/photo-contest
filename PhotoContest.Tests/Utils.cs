using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Services.Mapping;

namespace PhotoContest.Tests
{
    public class Utils
    {
        public static DbContextOptions<PhotoContestDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<PhotoContestDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        public static MapperConfiguration GetConfigurationMapper()
        {
            DTOMapper myProfile = new DTOMapper();
            return new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
        }
    }
}
