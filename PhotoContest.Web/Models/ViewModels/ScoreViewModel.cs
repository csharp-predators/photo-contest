﻿using System.Collections.Generic;

namespace PhotoContest.Web.Models.ViewModels
{
    public class ScoreViewModel
    {
        /// <summary>
        /// Gets or sets the total score.
        /// </summary>
        public int TotalScore { get; set; }

        /// <summary>
        /// Gets or sets the reviews.
        /// </summary>
        public ICollection<ReviewViewModel> Reviews { get; set; }
    }
}
