﻿using System.ComponentModel;

namespace PhotoContest.Web.Models.ViewModels
{
    public class UserViewModel
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Gets or sets the avatar path.
        /// </summary>
        [DisplayName("Profile Photo")]
        public string AvatarPath { get; set; }

        /// <summary>
        /// Gets or sets the points.
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets the points to next rank.
        /// </summary>
        public int PointsToNextRank { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public string Rank { get; set; }

        /// <summary>
        /// Gets or sets the JWT token.
        /// </summary>
        public string JwtToken { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is admin.
        /// </summary>
        public bool IsAdmin { get; set; }

    }
}