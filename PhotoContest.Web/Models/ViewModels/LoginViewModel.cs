﻿using PhotoContest.Services.DTOs;

namespace PhotoContest.Web.Models.ViewModels
{
    public class LoginViewModel
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Gets or sets the JWT token.
        /// </summary>
        public string JwtToken { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModel"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="jwtToken">The JWT token.</param>
        public LoginViewModel(UserDTO user, string jwtToken)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            Username = user.UserName;
            JwtToken = jwtToken;
        }
    }
}
