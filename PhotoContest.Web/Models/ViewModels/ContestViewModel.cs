﻿using System;
using System.ComponentModel;

namespace PhotoContest.Web.Models.ViewModels
{
    public class ContestViewModel
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public CategoryViewModel Category { get; set; }

        /// <summary>
        /// Gets or sets the creator.
        /// </summary>
        public UserViewModel Creator { get; set; }

        /// <summary>
        /// Gets or sets the phase.
        /// </summary>
        public string Phase { get; set; }

        /// <summary>
        /// Gets or sets the cover URL.
        /// </summary>
        [DisplayName("Cover")]
        public string CoverURL { get; set; }

        /// <summary>
        /// Gets or sets the phase one finish time.
        /// </summary>
        public DateTime PhaseOneFinishTime { get; set; }

        /// <summary>
        /// Gets or sets the phase two finish time.
        /// </summary>
        public DateTime PhaseTwoFinishTime { get; set; }

        /// <summary>
        /// Gets or sets the remaining time phase one.
        /// </summary>
        public TimeSpan RemainingTimePhaseOne { get; set; }

        /// <summary>
        /// Gets or sets the remaining time phase two.
        /// </summary>
        public TimeSpan RemainingTimePhaseTwo { get; set; }
    }
}
