﻿namespace PhotoContest.Web.Models.ViewModels
{
    public class CategoryViewModel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}
