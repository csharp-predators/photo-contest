﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Web.Models.InputModels
{
    public class ReviewCreateModel
    {
        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        [Required]
        [Range(1, 10)]
        public int Score { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 10)]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fits category].
        /// </summary>
        public bool FitsCategory { get; set; }

        /// <summary>
        /// Gets or sets the contest title.
        /// </summary>
        public string ContestTitle { get; set; }

        /// <summary>
        /// Gets or sets the contest identifier.
        /// </summary>
        public Guid ContestId { get; set; }

        /// <summary>
        /// Gets or sets the photo identifier.
        /// </summary>
        public Guid PhotoId { get; set; }

        /// <summary>
        /// Gets or sets the author identifier.
        /// </summary>
        public Guid AuthorId { get; set; }

    }
}