﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.Models.InputModels
{
 
    public class ImageViewModel
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets the cloudinary identifier.
        /// </summary>
        public string CloudinaryId { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        public IFormFile File { get; set; }
    }
}
