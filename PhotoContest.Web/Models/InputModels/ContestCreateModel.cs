﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Services.Helpers.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Web.Models.InputModels
{
    public class ContestCreateModel
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the cover identifier.
        /// </summary>
        public Guid CoverId { get; set; }

        /// <summary>
        /// Gets or sets the cover file.
        /// </summary>
        public IFormFile CoverFile { get; set; }

        /// <summary>
        /// Gets or sets the cover URL.
        /// </summary>
        [Url]
        public string CoverURL { get; set; }

        /// <summary>
        /// Gets or sets the name of the category.
        /// </summary>
        [Required]
        public string CategoryName { get; set; }

        /// <summary>
        /// Gets or sets the phase one finish time.
        /// </summary>
        [Required]
        [PhaseOneFinishTime]
        [DataType(DataType.Date)]
        public DateTime PhaseOneFinishTime { get; set; }

        /// <summary>
        /// Gets or sets the phase two finish time.
        /// </summary>
        [Required]
        [PhaseTwoFinishTime("PhaseOneFinishTime")]
        [DataType(DataType.Date)]
        public DateTime PhaseTwoFinishTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is invitational.
        /// </summary>
        public bool IsInvitational { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [master can be jury].
        /// </summary>
        public bool MasterCanBeJury { get; set; }

        /// <summary>
        /// Gets or sets the participants usernames.
        /// </summary>
        public ICollection<string> ParticipantsUsernames { get; set; }

        /// <summary>
        /// Gets or sets the jury usernames.
        /// </summary>
        public ICollection<string> JuryUsernames { get; set; }

    }
}
