﻿namespace PhotoContest.Web.Models.InputModels
{
    public class UniversalInputModel
    {
        /// <summary>
        /// Gets or sets the contest title.
        /// </summary>
        public string ContestTitle { get; set; }
    }
}