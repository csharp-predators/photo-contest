﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Web.Models.InputModels
{
    public class PhotoCreateModel
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [Required]
        [StringLength(20, MinimumLength = 3)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 10)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        [Required]
        public IFormFile File { get; set; }

        /// <summary>
        /// Gets or sets the contest title.
        /// </summary>
        public string ContestTitle { get; set; }
    }
}
