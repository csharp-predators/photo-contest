﻿using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Web.Models.InputModels
{
    public class LoginInputModel
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        [Required]
        [MinLength(2), MaxLength(20)]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required]
        [MinLength(8)]
        public string Password { get; set; }
    }
}
