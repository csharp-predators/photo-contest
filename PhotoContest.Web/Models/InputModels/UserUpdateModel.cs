﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Web.Models.InputModels
{
    public class UserUpdateModel
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [MinLength(2), MaxLength(20)]
        [DisplayName("First Name:")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [MinLength(2), MaxLength(20)]
        [DisplayName("Last Name:")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [EmailAddress]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the avatar file.
        /// </summary>
        public IFormFile AvatarFile { get; set; }
    }
}
