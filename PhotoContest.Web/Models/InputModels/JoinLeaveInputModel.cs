﻿using System;

namespace PhotoContest.Web.Models.InputModels
{
    public class JoinLeaveInputModel
    {
        /// <summary>
        /// Gets or sets the contest title.
        /// </summary>
        public string ContestTitle { get; set; }

        /// <summary>
        /// Gets or sets the participant identifier.
        /// </summary>
        public Guid ParticipantId { get; set; }
    }
}
