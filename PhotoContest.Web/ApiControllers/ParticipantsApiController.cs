﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Helpers.Attributes;
using PhotoContest.Web.Models.ViewModels;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("/api/[controller]")]
    [ApiController]
    [APIExceptionFilter]
    public class ParticipantsApiController : ControllerBase
    {
        #region Fields and Constructor

        /// <summary>
        /// The user service
        /// </summary>
        private readonly IUserService userService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParticipantsApiController"/> class.
        /// </summary>
        /// <param name="userService">The user service.</param>
        /// <param name="mapper">The mapper.</param>
        public ParticipantsApiController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> Get()
        {
            ICollection<UserDTO> participants = await userService.GetParticipantsAsync();
            List<UserViewModel> contestViewModels = mapper.Map<List<UserViewModel>>(participants);

            return Ok(contestViewModels);
        }

        /// <summary>
        /// Gets the profile.
        /// </summary>
        [HttpGet("profile")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Junkie")]
        public async Task<IActionResult> GetProfile()
        {
            string username = User.FindFirst(ClaimTypes.Name).Value;
            UserDTO userDTO = await userService.GetAsync(username);
            UserViewModel userViewModel = mapper.Map<UserViewModel>(userDTO);

            return Ok(userViewModel);
        }

        #endregion
    }
}
