﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Helpers.Attributes;
using PhotoContest.Web.Models.InputModels;
using PhotoContest.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("/api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [APIExceptionFilter]
    public class ContestsApiController : ControllerBase
    {
        #region Fields and Constructor

        /// <summary>
        /// The contest service
        /// </summary>
        private readonly IContestService contestService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The photo service
        /// </summary>
        private readonly IPhotoService photoService;
        /// <summary>
        /// The review service
        /// </summary>
        private readonly IReviewService reviewService;
        /// <summary>
        /// The validator service
        /// </summary>
        private readonly IValidatorService validatorService;
        /// <summary>
        /// The category service
        /// </summary>
        private readonly ICategoryService categoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContestsApiController"/> class.
        /// </summary>
        /// <param name="contestService">The contest service.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="photoService">The photo service.</param>
        /// <param name="reviewService">The review service.</param>
        /// <param name="validatorService">The validator service.</param>
        /// <param name="categoryService">The category service.</param>
        public ContestsApiController(IContestService contestService,
            IMapper mapper,
            IPhotoService photoService,
            IReviewService reviewService,
            IValidatorService validatorService,
            ICategoryService categoryService)
        {
            this.contestService = contestService;
            this.mapper = mapper;
            this.photoService = photoService;
            this.reviewService = reviewService;
            this.validatorService = validatorService;
            this.categoryService = categoryService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        public Guid UserId { get => Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value); }

        /// <summary>
        /// Adds the participant.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        [HttpPost("{contestTitle}/join")]
        public async Task<IActionResult> AddParticipant(string contestTitle)
        {
            await contestService.AddParticipantAsync(UserId, contestTitle);

            return Ok();
        }

        /// <summary>
        /// Creates the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        [HttpPost("create")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> Create([FromForm] ContestCreateModel model)
        {
            if (ModelState.IsValid)
            {
                ContestDTO dto = mapper.Map<ContestDTO>(model);
                dto.Category = await categoryService.GetAsync(model.CategoryName);
                dto.CreatorId = UserId;
                await contestService.CreateAsync(dto);

                return Ok();
            }

            return BadRequest();
        }

        /// <summary>
        /// Gets the by phase.
        /// </summary>
        /// <param name="phase">The phase.</param>
        [HttpGet("{phase}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> GetByPhase(string phase)
        {
            ICollection<ContestDTO> filteredContests = await contestService.GetByPhaseAsync(phase);
            List<ContestViewModel> contestViewModels = mapper.Map<List<ContestViewModel>>(filteredContests);

            return Ok(contestViewModels);
        }

        /// <summary>
        /// Gets the phase one.
        /// </summary>
        [HttpGet("open")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Junkie")]
        public async Task<IActionResult> GetPhaseOne()
        {
            ICollection<ContestDTO> openedContests = await contestService.GetByPhaseAsync("one");
            ICollection<ContestViewModel> contestsViewModels = mapper.Map<ICollection<ContestViewModel>>(openedContests);

            return Ok(contestsViewModels);
        }

        /// <summary>
        /// Gets the by participant.
        /// </summary>
        /// <param name="status">The status.</param>
        [HttpGet("my/{status}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Junkie")]
        public async Task<IActionResult> GetByParticipant([FromRoute] string status)
        {
            ICollection<ContestDTO> contestsDTOs = await contestService.GetByParticipantAndStatusAsync(UserId, status);
            ICollection<ContestViewModel> contestsViewModels = mapper.Map<ICollection<ContestViewModel>>(contestsDTOs);

            return Ok(contestsViewModels);
        }

        /// <summary>
        /// Gets the photos in contest.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        [HttpGet("{contestTitle}/photos")]
        public async Task<IActionResult> GetPhotosInContest(string contestTitle)
        {
            if (!await validatorService.CheckJuryAsync(contestTitle, UserId))
            {
                return Unauthorized(Constants.Exception.ONLY_JURY_ACCESS);
            }

            ICollection<PhotoDTO> photosDTOs = await photoService.GetByContestAsync(contestTitle);

            return Ok(photosDTOs);
        }

        /// <summary>
        /// Reviews the photo.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="contestTitle">The contest title.</param>
        /// <param name="photoId">The photo identifier.</param>
        [HttpPost("{contestTitle}/photos/{photoId}")]
        public async Task<IActionResult> ReviewPhoto([FromBody] ReviewCreateModel model, string contestTitle, string photoId)
        {
            if (!validatorService.ValidGuidString(photoId))
            {
                return BadRequest(Constants.Exception.INVALID_PHOTO_ID);
            }

            if (ModelState.IsValid)
            {
                if (await validatorService.CheckJuryAsync(contestTitle, UserId))
                {
                    ReviewDTO reviewDTO = mapper.Map<ReviewDTO>(model);
                    ContestDTO contestDTO = await contestService.GetAsync(contestTitle);
                    reviewDTO.ContestId = contestDTO.Id;
                    reviewDTO.PhotoId = Guid.Parse(photoId);
                    reviewDTO.AuthorId = UserId;

                    await reviewService.CreateAsync(reviewDTO);

                    return Ok();
                }

                return Unauthorized();
            }

            return BadRequest();
        }

        /// <summary>
        /// Gets the photo reviews.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <param name="photoId">The photo identifier.</param>
        [HttpGet("{contestTitle}/{photoId}")]
        public async Task<IActionResult> GetPhotoReviews(string contestTitle, string photoId)
        {
            if (validatorService.ValidGuidString(photoId))
            {
                return BadRequest(Constants.Exception.INVALID_PHOTO_ID);
            }

            if (await validatorService.CheckAdminAsync(UserId) ||
                await validatorService.CheckJuryAsync(contestTitle, UserId) ||
                await validatorService.CheckParticipantAsync(contestTitle, UserId))
            {
                ICollection<ReviewDTO> reviewDTOs = await reviewService
                    .GetByPhotoAsync(Guid.Parse(photoId), contestTitle);

                ICollection<ReviewViewModel> reviewViewModels = mapper.Map<ICollection<ReviewViewModel>>(reviewDTOs);

                int totalScore = reviewViewModels.Sum(r => r.Score);

                ScoreViewModel scoreViewModel = new ScoreViewModel
                {
                    TotalScore = totalScore,
                    Reviews = reviewViewModels
                };

                return Ok(scoreViewModel);
            }

            return Unauthorized(Constants.Exception.UNAUTHORIZED_ACCESS);
        }

        /// <summary>
        /// Uploads the photo.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="contestTitle">The contest title.</param>
        [HttpPost("{contestTitle}/photos/upload")]
        public async Task<IActionResult> UploadPhoto([FromForm] PhotoCreateModel model, string contestTitle)
        {
            if (ModelState.IsValid)
            {
                if (await validatorService.CheckParticipantAsync(contestTitle, UserId))
                {
                    PhotoDTO photoDTO = mapper.Map<PhotoDTO>(model);
                    photoDTO.AuthorId = UserId;

                    await photoService.AddPhotoAsync(photoDTO, contestTitle);

                    return Ok();
                }

                return Unauthorized();
            }

            return BadRequest();
        }

        #endregion
    }
}
