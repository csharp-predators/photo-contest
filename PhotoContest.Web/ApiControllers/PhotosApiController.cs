﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Helpers.Attributes;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [APIExceptionFilter]
    public class PhotosApiController : ControllerBase
    {
        #region Fields and Constructor

        /// <summary>
        /// The photo service
        /// </summary>
        private readonly IPhotoService photoService;
        /// <summary>
        /// The cloudinary service
        /// </summary>
        private readonly ICloudinaryService cloudinaryService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The user service
        /// </summary>
        private readonly IUserService userService;
        /// <summary>
        /// The validator service
        /// </summary>
        private readonly IValidatorService validatorService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhotosApiController"/> class.
        /// </summary>
        /// <param name="photoService">The photo service.</param>
        /// <param name="cloudinaryService">The cloudinary service.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="validatorService">The validator service.</param>
        public PhotosApiController(IPhotoService photoService,
            ICloudinaryService cloudinaryService,
            IMapper mapper,
            IUserService userService,
            IValidatorService validatorService)
        {
            this.photoService = photoService;
            this.cloudinaryService = cloudinaryService;
            this.mapper = mapper;
            this.userService = userService;
            this.validatorService = validatorService;
        }

        #endregion

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        public Guid UserId { get => Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value); }

        // api/photos/
        // Think about move to ContestsController
        /// <summary>
        /// Deletes the specified photo identifier.
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        [HttpDelete("{photoId}")]
        public async Task<IActionResult> Delete(string photoId)
        {
            if (validatorService.ValidGuidString(photoId))
            {
                return BadRequest(Constants.Exception.INVALID_PHOTO_ID);
            }

            if (await validatorService.CheckPhotoAuthorAsync(Guid.Parse(photoId), UserId)
                || await validatorService.CheckAdminAsync(UserId))
            {
                await photoService.DeletePhotoAsync(Guid.Parse(photoId));
                return Ok();
            }

            return Unauthorized(Constants.Exception.UNAUTHORIZED_ACCESS);
        }
    }
}
