﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Helpers.Attributes;
using PhotoContest.Web.Models.InputModels;
using PhotoContest.Web.Models.ViewModels;
using System.Threading.Tasks;

namespace PhotoContest.Web.ApiControllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("/api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [APIExceptionFilter]
    public class UsersApiController : ControllerBase
    {
        #region Fields and Constructor 

        /// <summary>
        /// The user service
        /// </summary>
        private readonly IUserService userService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersApiController"/> class.
        /// </summary>
        /// <param name="userService">The user service.</param>
        /// <param name="mapper">The mapper.</param>
        public UsersApiController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Login user
        /// </summary>
        /// <param name="model">The model.</param>
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginInputModel model)
        {
            if (ModelState.IsValid)
            {
                LoginDTO loginDTO = mapper.Map<LoginDTO>(model);
                UserDTO userDTO = await userService.LoginAsync(loginDTO);
                UserViewModel userViewModel = mapper.Map<UserViewModel>(userDTO);

                return Ok(userViewModel);
            }

            return BadRequest();
        }

        /// <summary>
        /// Registers new user.
        /// </summary>
        /// <param name="model">The model.</param>
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterInputModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDTO = mapper.Map<UserDTO>(model);
                UserDTO responseUserDTO = await userService.CreateAsync(userDTO, model.Password);
                UserViewModel userViewModel = mapper.Map<UserViewModel>(responseUserDTO);

                return Ok(userViewModel);
            }

            return BadRequest();
        }

        #endregion
    }
}
