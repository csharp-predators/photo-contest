﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(PhotoContest.Web.Areas.Identity.IdentityHostingStartup))]
namespace PhotoContest.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
            });
        }
    }
}