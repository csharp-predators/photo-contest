﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PhotoContest.Data;
using PhotoContest.Services.Common;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoContest.Web.Helpers.HostedServices
{
    /// <summary>
    /// Hosted service who changes runtime user rank.
    /// </summary>
    /// <seealso cref="Microsoft.Extensions.Hosting.IHostedService" />
    /// <seealso cref="System.IDisposable" />
    public class RankHostedService : IHostedService, IDisposable
    {
        /// <summary>
        /// The scope factory
        /// </summary>
        private readonly IServiceScopeFactory scopeFactory;
        /// <summary>
        /// The execution count
        /// </summary>
        private int executionCount = 0;
        /// <summary>
        /// The timer
        /// </summary>
        private Timer _timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="RankHostedService"/> class.
        /// </summary>
        /// <param name="scopeFactory">The scope factory.</param>
        public RankHostedService(IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        /// <summary>
        /// Starts the asynchronous.
        /// </summary>
        /// <param name="stoppingToken">The stopping token.</param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken stoppingToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(30));

            return Task.CompletedTask;
        }

        /// <summary>
        /// Does the work.
        /// </summary>
        /// <param name="state">The state.</param>
        private void DoWork(object state)
        {
            int count = Interlocked.Increment(ref executionCount);

            using (IServiceScope scope = scopeFactory.CreateScope())
            {
                PhotoContestDbContext context = scope.ServiceProvider.GetRequiredService<PhotoContestDbContext>();

                foreach (Data.Models.User participant in context.Users.Where(p => !p.IsAdmin))
                {
                    int points = participant.Points;

                    if (points < Constants.Participant.MIN_POINTS_ENTHUSIAST)
                    {
                        participant.Rank = Constants.Participant.RANK_JUNKIE;
                    }
                    else if (points < Constants.Participant.MIN_POINTS_MASTER)
                    {
                        participant.Rank = Constants.Participant.RANK_Enthusiast;
                    }
                    else if (points < Constants.Participant.MIN_POINTS_DICTATOR)
                    {
                        participant.Rank = Constants.Participant.RANK_MASTER;
                    }
                    else
                    {
                        participant.Rank = Constants.Participant.RANK_Dictator;
                    }
                }

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Stops the asynchronous.
        /// </summary>
        /// <param name="stoppingToken">The stopping token.</param>
        /// <returns></returns>
        public Task StopAsync(CancellationToken stoppingToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
