﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoContest.Web.Helpers.HostedServices
{
    /// <summary>
    /// Hosted service who changes runtime contest phase.
    /// </summary>
    /// <seealso cref="Microsoft.Extensions.Hosting.IHostedService" />
    /// <seealso cref="System.IDisposable" />
    public class PhaseHostedService : IHostedService, IDisposable
    {
        /// <summary>
        /// The scope factory
        /// </summary>
        private readonly IServiceScopeFactory scopeFactory;
        /// <summary>
        /// The execution count
        /// </summary>
        private int executionCount = 0;
        /// <summary>
        /// The timer
        /// </summary>
        private Timer _timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhaseHostedService"/> class.
        /// </summary>
        /// <param name="scopeFactory">The scope factory.</param>
        public PhaseHostedService(IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        /// <summary>
        /// Triggered when the application host is ready to start the service.
        /// </summary>
        /// <param name="cancellationToken">Indicates that the start process has been aborted.</param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(60));

            return Task.CompletedTask;
        }

        /// <summary>
        /// Does the work.
        /// </summary>
        /// <param name="state">The state.</param>
        private void DoWork(object state)
        {
            int count = Interlocked.Increment(ref executionCount);

            using (IServiceScope scope = scopeFactory.CreateScope())
            {
                PhotoContestDbContext context = scope.ServiceProvider.GetRequiredService<PhotoContestDbContext>();

                List<Contest> contests = context.Contests
                    .Include(c => c.Photos)
                    .Include(c => c.Jury)
                    .Include(c => c.Participants)
                    .Where(c => c.Phase != Constants.Contest.PHASE_FINISHED_STRING)
                    .ToList();

                IContestService contestService = scope.ServiceProvider.GetRequiredService<IContestService>();

                foreach (Contest contest in contests)
                {
                    if (DateTime.Compare(contest.PhaseOneFinishTime, DateTime.Now) >= 0)
                    {
                        contest.Phase = Constants.Contest.PHASE_ONE_STRING;
                    }
                    else if (DateTime.Compare(contest.PhaseTwoFinishTime, DateTime.Now) >= 0 && 
                        contest.Phase == string.Format(Constants.Contest.PHASE_ONE_STRING, contest.Title))
                    {
                        contest.Phase = Constants.Contest.PHASE_TWO_STRING;
                        contestService.NotificatePhaseTwoStarts(contest);
                    }
                    else if(DateTime.Compare(contest.PhaseTwoFinishTime, DateTime.Now) <= 0 &&
                        contest.Phase == Constants.Contest.PHASE_TWO_STRING)
                    {
                        contest.Phase = Constants.Contest.PHASE_FINISHED_STRING;

                        if (contest.Photos.Count > 0)
                        {
                            contestService.AwardWinnersPoints(contest.Title);
                            contestService.NotificateFinish(contest);
                        }
                    }
                }

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Triggered when the application host is performing a graceful shutdown.
        /// </summary>
        /// <param name="cancellationToken">Indicates that the shutdown process should no longer be graceful.</param>
        /// <returns></returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
