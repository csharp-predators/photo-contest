﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace PhotoContest.Web.Helpers.Attributes
{
    /// <summary>
    /// Exception filter for API controllers
    /// </summary>
    /// <seealso cref="System.Attribute" />
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Filters.IExceptionFilter" />
    public class APIExceptionFilter : Attribute, IExceptionFilter
    {
        /// <summary>
        /// Called after an action has thrown an <see cref="T:System.Exception" />.
        /// </summary>
        /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ExceptionContext" />.</param>
        public void OnException(ExceptionContext context)
        {
            Exception exception = context.Exception;

            if (exception is ArgumentException argumentException)
            {
                context.Result = new ContentResult()
                {
                    Content = argumentException.Message,
                    StatusCode = 400,
                };
            }
            else if (exception is ArgumentOutOfRangeException outOfRangeException)
            {
                context.Result = new ContentResult()
                {
                    Content = outOfRangeException.Message,
                    StatusCode = 406,
                };
            }
            else if (exception is InvalidOperationException invalidOperationException)
            {
                context.Result = new ContentResult()
                {
                    Content = invalidOperationException.Message,
                    StatusCode = 405,
                };
            }
            else
            {
                context.Result = new ContentResult()
                {
                    Content = exception.Message,
                    StatusCode = 500,
                };
            }
        }
    }
}
