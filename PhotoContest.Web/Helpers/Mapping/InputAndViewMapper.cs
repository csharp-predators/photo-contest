﻿using AutoMapper;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Models.InputModels;
using PhotoContest.Web.Models.ViewModels;

namespace PhotoContest.Services.Mapping
{
    /// <summary>
    /// Configuration profile for Automapper
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class InputAndViewMapper : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InputAndViewMapper"/> class.
        /// </summary>
        public InputAndViewMapper()
        {
            CreateMap<ContestCreateModel, ContestDTO>().ReverseMap();
            CreateMap<ContestViewModel, ContestDTO>().ReverseMap();
            CreateMap<ContestDTO, ContestViewModel>().ForMember(x => x.CoverURL, z => z.MapFrom(y => y.Cover.ImageUrl));

            CreateMap<UserViewModel, UserDTO>();
            CreateMap<UserDTO, UserViewModel>().ForMember(x => x.AvatarPath, z => z.MapFrom(y => y.Avatar.ImageUrl));

            CreateMap<CategoryViewModel, CategoryDTO>().ReverseMap();

            CreateMap<ReviewCreateModel, ReviewDTO>().ReverseMap();

            CreateMap<PhotoCreateModel, PhotoDTO>().ReverseMap();
            CreateMap<PhotoViewModel, PhotoDTO>().ReverseMap();

            CreateMap<ReviewDTO, ReviewViewModel>().ForMember(x => x.Author, z => z.MapFrom(y => y.Author.UserName));

            CreateMap<LoginInputModel, LoginDTO>().ReverseMap();

            CreateMap<UserDTO, RegisterInputModel>().ReverseMap();

            CreateMap<ImageDTO, ImageViewModel>();

            CreateMap<UserUpdateModel, UserDTO>();
            CreateMap<UserDTO, UserUpdateModel>();
        }
    }
}
