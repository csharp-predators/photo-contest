﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PhotoContest.Services.Common;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Helpers.Enums;
using PhotoContest.Web.Models.InputModels;
using PhotoContest.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;

namespace PhotoContest.Web.Controllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Authorize]
    public class ContestsController : Controller
    {
        #region Fields and Constructor

        /// <summary>
        /// The contest service
        /// </summary>
        private readonly IContestService contestService;
        /// <summary>
        /// The user service
        /// </summary>
        private readonly IUserService userService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The photo service
        /// </summary>
        private readonly IPhotoService photoService;
        /// <summary>
        /// The review service
        /// </summary>
        private readonly IReviewService reviewService;
        /// <summary>
        /// The category service
        /// </summary>
        private readonly ICategoryService categoryService;
        /// <summary>
        /// The toast notification
        /// </summary>
        private readonly IToastNotification toastNotification;
        /// <summary>
        /// The notification service
        /// </summary>
        private readonly INotificationService notificationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContestsController"/> class.
        /// </summary>
        /// <param name="contestService">The contest service.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="photoService">The photo service.</param>
        /// <param name="reviewService">The review service.</param>
        /// <param name="categoryService">The category service.</param>
        /// <param name="toastNotification">The toast notification.</param>
        /// <param name="notificationService">The notification service.</param>
        public ContestsController(IContestService contestService,
            IUserService userService,
            IMapper mapper,
            IPhotoService photoService,
            IReviewService reviewService,
            ICategoryService categoryService,
            IToastNotification toastNotification,
            INotificationService notificationService)
        {
            this.contestService = contestService;
            this.userService = userService;
            this.mapper = mapper;
            this.photoService = photoService;
            this.reviewService = reviewService;
            this.categoryService = categoryService;
            this.toastNotification = toastNotification;
            this.notificationService = notificationService;
        }

        #endregion

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        public Guid UserId { get => Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)); }

        #region Actions

        /// <summary>
        /// Receives photo create form and create it.
        /// </summary>
        /// <param name="model">The model.</param>
        [HttpPost]
        [Authorize(Roles = "Junkie")]
        public async Task<IActionResult> AddPhoto([FromForm] PhotoCreateModel model)
        {
            if (ModelState.IsValid)
            {
                PhotoDTO photoDTO = mapper.Map<PhotoDTO>(model);
                photoDTO.AuthorId = UserId;

                await photoService.AddPhotoAsync(photoDTO, model.ContestTitle);

                return RedirectToAction("Contest", new { contestTitle = model.ContestTitle });
            }

            // TODO:
            return BadRequest();
        }

        /// <summary>
        /// Add photo view.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        [HttpGet]
        [Authorize(Roles = "Junkie")]
        public IActionResult AddPhoto(string contestTitle)
        {
            ViewBag.ContestTitle = contestTitle;
            return View();
        }

        /// <summary>
        /// Add review view.
        /// </summary>
        /// <param name="model">The model.</param>
        [HttpPost]
        public async Task<IActionResult> AddReview([FromForm] ReviewCreateModel model)
        {
            if (ModelState.IsValid)
            {
                if (!model.FitsCategory)
                {
                    model.Score = 0;
                }

                model.AuthorId = UserId;
                ReviewDTO reviewDTO = mapper.Map<ReviewDTO>(model);
                ContestDTO contest = await contestService.GetAsync(model.ContestTitle);
                reviewDTO.ContestId = contest.Id;
                await reviewService.CreateAsync(reviewDTO);

                return RedirectToAction("Contest", new { contestTitle = model.ContestTitle });
            }

            return BadRequest();
        }

        /// <summary>
        /// Returns view with all contests listed.
        /// </summary>
        /// <param name="phase">The phase.</param>
        /// <param name="page">The page.</param>
        /// <param name="search">The search.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> All(string phase, int? page, string search, SortBy? sortBy, OrderBy? orderBy)
        {
            ViewBag.Search = search;
            ViewBag.SortBy = sortBy;
            ViewBag.OrderBy = orderBy;
            ViewBag.Phase = phase;

            if (phase != null)
            {
                ICollection<ContestDTO> contestsByPhase = await contestService.GetByPhaseAsync(phase, search, sortBy, orderBy);

                ICollection<ContestViewModel> contestsByPhaseViewModels = mapper.Map<ICollection<ContestViewModel>>(contestsByPhase);
                IPagedList<ContestViewModel> pageModelByPhase = await contestsByPhaseViewModels.ToPagedListAsync(page ?? 1, 8);

                return View(pageModelByPhase);
            }

            ICollection<ContestDTO> contests = await contestService.GetAllAsync(search, sortBy, orderBy);

            ICollection<ContestViewModel> contestsViewModels = mapper.Map<ICollection<ContestViewModel>>(contests);
            IPagedList<ContestViewModel> pageModel = await contestsViewModels.ToPagedListAsync(page ?? 1, 8);

            return View(pageModel);
        }

        [HttpGet]
        [Authorize(Roles = "Junkie")]
        public async Task<IActionResult> FinishedContests(int? page, string search, SortBy? sortBy, OrderBy? orderBy)
        {
            ViewBag.Search = search;
            ViewBag.SortBy = sortBy;
            ViewBag.OrderBy = orderBy;

            ICollection<ContestDTO> contestsByPhase = await contestService.GetByPhaseAsync("finished", search, sortBy, orderBy);
            
            ICollection<ContestViewModel> contestsByPhaseViewModels = mapper.Map<ICollection<ContestViewModel>>(contestsByPhase);
            IPagedList<ContestViewModel> pageModelByPhase = await contestsByPhaseViewModels.ToPagedListAsync(page ?? 1, 5);

            return View(pageModelByPhase);
        }

        /// <summary>
        /// Returns page of contest with info for it
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        [HttpGet]
        public async Task<IActionResult> Contest(string contestTitle)
        {
            ContestDTO contestDTO = await contestService.GetAsync(contestTitle);
            ContestViewModel model = mapper.Map<ContestViewModel>(contestDTO);
            model.CoverURL = contestDTO.Cover.ImageUrl;

            return View("Contest", model);
        }

        /// <summary>
        /// Create contest view.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var imagesDTOs = await photoService.GetImagesAsync();
            var imagesViews = mapper.Map<ICollection<ImageViewModel>>(imagesDTOs);

            var usersDTOs = await userService.GetAsync();
            var usersViews = mapper.Map<ICollection<UserViewModel>>(usersDTOs);

            ViewBag.Users = usersViews.Where(u => u.IsAdmin == false);
            ViewBag.Images = imagesViews;

            return View();
        }

        /// <summary>
        /// Creates the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(ContestCreateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ContestDTO contestDTO = mapper.Map<ContestDTO>(model);
                    var category = await categoryService.GetCategoryAsync(model.CategoryName);
                    if (category == null)
                    {
                        category = new CategoryDTO() { Name = model.CategoryName };
                        await categoryService.CreateAsync(category);
                    }

                    contestDTO.CreatorId = UserId;
                    contestDTO.CategoryId = category.Id;
                    await contestService.CreateAsync(contestDTO);
                }
            }
            catch (Exception)
            {
                toastNotification.AddErrorToastMessage("Already existing contest Title. Please add non-existing Title.");
                return RedirectToAction("Create");
            }

            toastNotification.AddSuccessToastMessage("Great! You have successfully created contest.");
            return RedirectToAction("All");
        }

        /// <summary>
        /// Removes notification for contest and redirects to add photo page before join it.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <param name="notificationId">The notification identifier.</param>
        [HttpGet]
        [Authorize(Roles = "Junkie")]
        public async Task<IActionResult> NotificatedAddPhoto(string contestTitle, Guid notificationId)
        {
            await notificationService.MarkNoticationAsReadedAsync(notificationId);
            ViewBag.ContestTitle = contestTitle;

            return View("AddPhoto");
        }

        /// <summary>
        /// Removes notification for contest and redirects to it.
        /// </summary>
        /// <param name="contestTitle">The contest title.</param>
        /// <param name="notificationId">The notification identifier.</param>
        [HttpGet]
        public async Task<IActionResult> NotificatedContest(string contestTitle, Guid notificationId)
        {
            ContestDTO contestDTO = await contestService.GetAsync(contestTitle);
            ContestViewModel model = mapper.Map<ContestViewModel>(contestDTO);
            model.CoverURL = contestDTO.Cover.ImageUrl;
            await notificationService.MarkNoticationAsReadedAsync(notificationId);

            return View("Contest", model);
        }

        /// <summary>
        /// Joins the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        [Authorize(Roles = "Junkie")]
        [HttpGet]
        public async Task<IActionResult> Join(JoinLeaveInputModel model)
        {
            await contestService.AddParticipantAsync(model.ParticipantId, model.ContestTitle);

            return RedirectToAction("AddPhoto", new { contestTitle = model.ContestTitle });
        }

        /// <summary>
        /// Returns view with all contests in which logged user is jury.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="search">The search.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        [Authorize(Roles = "Junkie")]
        [HttpGet]
        public async Task<IActionResult> JuryContests(int? page, string search, SortBy? sortBy, OrderBy? orderBy)
        {
            ViewBag.Search = search;
            ViewBag.SortBy = sortBy;
            ViewBag.OrderBy = orderBy;

            ICollection<ContestViewModel> openedContests = mapper
                .Map<ICollection<ContestViewModel>>(await contestService.GetByJuryAsync(UserId, search, sortBy, orderBy));

            IPagedList<ContestViewModel> pageModel = await openedContests.ToPagedListAsync(page ?? 1, 8);

            return View(pageModel);
        }

        /// <summary>
        /// Returns view with all contests which are open for joining.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="search">The search.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        [Authorize(Roles = "Junkie")]
        [HttpGet]
        public async Task<IActionResult> OpenContests(int? page, string search, SortBy? sortBy, OrderBy? orderBy)
        {
            ViewBag.Search = search;
            ViewBag.SortBy = sortBy;
            ViewBag.OrderBy = orderBy;

            ICollection<ContestViewModel> openedContests = mapper
                .Map<ICollection<ContestViewModel>>(await contestService.GetByPhaseAsync("one", search, sortBy, orderBy));

            IPagedList<ContestViewModel> pageModel = await openedContests.ToPagedListAsync(page ?? 1, 8);

            return View(pageModel);
        }

        /// <summary>
        /// Returns view with current contests of current logged user.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="search">The search.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        [Authorize(Roles = "Junkie")]
        [HttpGet]
        public async Task<IActionResult> ParticipatingContests(int? page, string search, SortBy? sortBy, OrderBy? orderBy)
        {
            ViewBag.Search = search;
            ViewBag.SortBy = sortBy;
            ViewBag.OrderBy = orderBy;

            ICollection<ContestViewModel> currentParticipateContests = mapper
                .Map<ICollection<ContestViewModel>>(await contestService.GetByParticipantAndStatusAsync(UserId, "current", search, sortBy, orderBy));

            IPagedList<ContestViewModel> pageModel = await currentParticipateContests.ToPagedListAsync(page ?? 1, 8);

            return View(pageModel);
        }

        /// <summary>
        /// Returns view with finished contests of current logged user.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="search">The search.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        [Authorize(Roles = "Junkie")]
        [HttpGet]
        public async Task<IActionResult> ParticipatedContests(int? page, string search, SortBy? sortBy, OrderBy? orderBy)
        {
            ViewBag.Search = search;
            ViewBag.SortBy = sortBy;
            ViewBag.OrderBy = orderBy;

            ICollection<ContestViewModel> participatedContests = mapper
                .Map<ICollection<ContestViewModel>>(await contestService.GetByParticipantAndStatusAsync(UserId, "finished", search, sortBy, orderBy));

            IPagedList<ContestViewModel> pageModel = await participatedContests.ToPagedListAsync(page ?? 1, 8);

            return View(pageModel);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult GetContestCreateModel(ContestCreateModel model)
        {
            return RedirectToAction("SetJury", model);
        }

        /// <summary>
        /// Returns view for adding jury to contest.
        /// </summary>
        /// <param name="model">The model.</param>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> SetJury(ContestCreateModel model)
        {
            if (ModelState.IsValid)
            {
                var usersDTOs = await userService.GetAsync();
                var usersViews = mapper.Map<ICollection<UserViewModel>>(usersDTOs);
                if (model.MasterCanBeJury)
                {
                    if (model.ParticipantsUsernames == null)
                        ViewBag.AvailableParticipantsToBeJury = usersViews
                        .Where(u => u.Rank == Constants.Participant.RANK_MASTER || u.Rank == Constants.Participant.RANK_Dictator);
                    else
                        ViewBag.AvailableParticipantsToBeJury = usersViews
                            .Where(u => u.Rank == Constants.Participant.RANK_MASTER || u.Rank == Constants.Participant.RANK_Dictator)
                            .Where(u => !model.ParticipantsUsernames.Contains(u.UserName));
                }
                else
                {
                    if (model.ParticipantsUsernames == null)
                        ViewBag.AvailableParticipantsToBeJury = usersViews
                        .Where(u => u.Rank == Constants.Participant.RANK_Dictator);
                    else
                        ViewBag.AvailableParticipantsToBeJury = usersViews
                            .Where(u => u.Rank == Constants.Participant.RANK_Dictator)
                            .Where(u => !model.ParticipantsUsernames.Contains(u.UserName));
                }

                return View(model);
            }

            toastNotification.AddErrorToastMessage("Invalid parameters.");
            return BadRequest();
        }

        #endregion
    }
}
