﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Web.Models.InputModels;
using PhotoContest.Web.Models.ViewModels;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PhotoContest.Web.Controllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Authorize]
    public class UsersController : Controller
    {
        /// <summary>
        /// The user service
        /// </summary>
        private readonly IUserService userService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The photo service
        /// </summary>
        private readonly IPhotoService photoService;
        /// <summary>
        /// The toast notification
        /// </summary>
        private readonly IToastNotification toastNotification;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersController"/> class.
        /// </summary>
        /// <param name="userService">The user service.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="photoService">The photo service.</param>
        /// <param name="toastNotification">The toast notification.</param>
        public UsersController(IUserService userService, IMapper mapper, IPhotoService photoService, IToastNotification toastNotification)
        {
            this.userService = userService;
            this.mapper = mapper;
            this.photoService = photoService;
            this.toastNotification = toastNotification;
        }

        /// <summary>
        /// Action for logged user profile.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            string username = User.FindFirst(ClaimTypes.Name).Value;
            UserDTO userDTO = await userService.GetAsync(username);
            UserViewModel userViewModel = mapper.Map<UserViewModel>(userDTO);
            ICollection<PhotoDTO> userPhotos = await photoService.GetPhotosByParticipantAsync(username);
            float percentageProgressRank = (float)(1.0 * userViewModel.Points / (userViewModel.Points + userViewModel.PointsToNextRank)) * 100;
            string percentNumber = percentageProgressRank.ToString();

            ViewBag.UserPhotos = userPhotos;
            ViewBag.PercentNumber = percentNumber;

            return View(userViewModel);
        }

        /// <summary>
        /// Action for user profile
        /// </summary>
        /// <param name="username">The username.</param>
        [HttpGet]
        public async Task<IActionResult> UserProfile(string username)
        {
            UserDTO userDTO = await userService.GetAsync(username);
            UserViewModel userViewModel = mapper.Map<UserViewModel>(userDTO);
            ICollection<PhotoDTO> userPhotos = await photoService.GetPhotosByParticipantAsync(username);
            float percentageProgressRank = (float)(1.0 * userViewModel.Points / (userViewModel.Points + userViewModel.PointsToNextRank)) * 100;
            string percentNumber = percentageProgressRank.ToString();

            ViewBag.UserPhotos = userPhotos;
            ViewBag.PercentNumber = percentNumber;

            return View("ProfileUsername", userViewModel);
        }

        /// <summary>
        /// Renders view for Update form.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Update()
        {
            string user = User.FindFirst(ClaimTypes.Name).Value;
            UserDTO userDTO = await userService.GetAsync(user);
            UserViewModel userViewModel = mapper.Map<UserViewModel>(userDTO);
            UserUpdateModel userUpdateModel = mapper.Map<UserUpdateModel>(userDTO);
            ViewBag.Participant = userViewModel;

            return View(userUpdateModel);
        }

        /// <summary>
        /// Receives model from update form.
        /// </summary>
        /// <param name="model">The model.</param>
        [HttpPost]
        public async Task<IActionResult> Update(UserUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                string participantName = User.FindFirst(ClaimTypes.Name).Value;
                UserDTO participantDTO = mapper.Map<UserDTO>(model);

                await userService.UpdateAsync(participantName, participantDTO);

                toastNotification.AddSuccessToastMessage("Greate! You have successfully updated your profile.");
                return RedirectToAction("Profile");
            }

            return BadRequest();
        }
    }
}
