﻿using Microsoft.AspNetCore.Mvc;

namespace PhotoContest.Web.Controllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    public class ErrorsController : Controller
    {
        /// <summary>
        /// Returns error view.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        [Route("/Error/{statusCode}")]
        [HttpGet]
        public IActionResult Error(int statusCode)
        {
            int a = 0;

            return statusCode switch
            {
                404 => View("NotFound"),
                403 => View("Unautorized"),
                500 => View("BadRequest"),
                _ => View("ServerError"),
            };
        }
    }
}