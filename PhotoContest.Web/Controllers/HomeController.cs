﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Models;
using PhotoContest.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PhotoContest.Web.Controllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    public class HomeController : Controller
    {
        /// <summary>
        /// The photo service
        /// </summary>
        private readonly IPhotoService photoService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="photoService">The photo service.</param>
        /// <param name="mapper">The mapper.</param>
        public HomeController(IPhotoService photoService, IMapper mapper)
        {
            this.photoService = photoService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        public Guid UserId { get => Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)); }

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var photosDTOs = await photoService.GetLatestWinningPhotosAsync();
            var photosViews = mapper.Map<ICollection<PhotoViewModel>>(photosDTOs);
            ViewBag.TopPhotos = photosViews;
            ViewBag.FirstPhoto = photosViews.First().ImageUrl;
            ViewBag.SecondPhoto = photosViews.Skip(1).First().ImageUrl;
            ViewBag.ThirdPhoto = photosViews.Skip(2).First().ImageUrl;
            ViewBag.FourthPhoto = photosViews.Skip(3).First().ImageUrl;
            ViewBag.FifthPhoto = photosViews.Skip(4).First().ImageUrl;
            ViewBag.SixthPhoto = photosViews.Skip(5).First().ImageUrl;

            return View();
        }
        /// <summary>
        /// Privacies this instance.
        /// </summary>
        [HttpGet]
        public IActionResult Privacy()
        {
            return View();
        }

        /// <summary>
        /// Ruleses this instance.
        /// </summary>
        [HttpGet]
        public IActionResult Rules()
        {
            return View();
        }

        /// <summary>
        /// Errors this instance.
        /// </summary>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
