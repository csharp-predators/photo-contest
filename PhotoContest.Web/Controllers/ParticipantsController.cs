﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.Helpers.Enums;
using PhotoContest.Web.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using X.PagedList;

namespace PhotoContest.Web.Controllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Authorize]
    public class ParticipantsController : Controller
    {
        /// <summary>
        /// The user service
        /// </summary>
        private readonly IUserService userService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParticipantsController"/> class.
        /// </summary>
        /// <param name="userService">The user service.</param>
        /// <param name="mapper">The mapper.</param>
        public ParticipantsController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Returns view with all participants.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="search">The search.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="orderBy">The order by.</param>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> All(int? page, string search, SortByUser? sortBy, OrderBy? orderBy)
        {
            ViewBag.Search = search;
            ViewBag.SortBy = sortBy;
            ViewBag.OrderBy = orderBy;

            ICollection<Services.DTOs.UserDTO> participants = await userService.GetParticipantsAsync(search, sortBy, orderBy);

            ICollection<UserViewModel> participantsViewModels = mapper.Map<ICollection<UserViewModel>>(participants);
            IPagedList<UserViewModel> pageModel = await participantsViewModels.ToPagedListAsync(page ?? 1, 5);

            return View(pageModel);
        }
    }
}
