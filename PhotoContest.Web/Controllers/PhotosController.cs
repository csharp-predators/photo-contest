﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Services.Contracts;
using PhotoContest.Web.Models.ViewModels;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Web.Controllers
{
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Authorize]
    public class PhotosController : Controller
    {
        #region Fields and Constructor

        /// <summary>
        /// The photo service
        /// </summary>
        private readonly IPhotoService photoService;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhotosController"/> class.
        /// </summary>
        /// <param name="photoService">The photo service.</param>
        /// <param name="mapper">The mapper.</param>
        public PhotosController(IPhotoService photoService,
            IMapper mapper)
        {
            this.photoService = photoService;
            this.mapper = mapper;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Photo view action
        /// </summary>
        /// <param name="photoId">The photo identifier.</param>
        [HttpGet("photos/{photoId}")]
        public async Task<IActionResult> Photo(Guid photoId)
        {
            var photoDTO = await photoService.GetPhotoAsync(photoId);
            var model = mapper.Map<PhotoViewModel>(photoDTO);

            return View(model);
        }

        #endregion
    }
}
