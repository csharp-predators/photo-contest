# Photo Contest

Created by: 
- **[Krastyu Terziev](https://gitlab.com/krastyuterziev)**
- **[Vasil Evlogiev](https://gitlab.com/vasilevlogiev)**

Trello Link: [Trello](https://trello.com/b/IwBZjG9o/photocontest)

Git Link: [GitLab](https://gitlab.com/csharp-predators/photo-contest)

**Application for aspiring photographers who want an application that can allow them to easily manage online photo contests.**

![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622196921/550-5500416_camera-logo-png-camera-logo-black-and-white_wx0ldv.png)


# Technologies
***

- ASP .NET Core
- ASP .NET Core Identity
- Entity Framework Core
- MS SQL Server
- SQL
- SSMS
- Automapper
- RESTful API
- JWT Token
- Azure
- HTML 5
- CSS 3
- Razor
- MOQ

# Database relations
***
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622894508/screenshots/Screenshot_2021-06-05_150108_bnxbp2.png)

# Administrative part
***
Admins are responsible for managing the application. Their responsibilities are:

```
1. Create new contest
2. By default every admin is a jury for each contest
3. View contests in Phase One, Phase Two and Finished
4. View list of all photographers
5. Edit Profile
```

# Users part
***
The mission of the Photo Contest app is to allow photographers to sign up to contests. 
Apart from that, every photographer should be able to:

```
1. Join contests
2. View open contests
3. Participate in contest like jury when invited for that role from admin
4. View finished contests
5. View current contests in which it participates
6. View its rank and points
7. Edit Profile
8. Upload photo to contest
9. Receive points for finishing in top 3
10. View reviews for its photos
11. View profiles of other users and admins
12. View photos of other users
```

# Areas
***
- Public - accessible without authentication
- Private - available after registration

# Public Part
***

The public part of our application should be accessible without authentication. This includes the application start page, the user login and user registration forms.
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622892867/screenshots/Screenshot_2021-06-05_143418_uewm6z.png)

People that are not authenticated cannot see any user-specific activities, they can only fill up the registration form or log in.  

- **Login** : The newly created user can log in to our app by using email and password.
- **Registration** : It requires filling the more information about the upcoming user: username, fist name, last name, email and password. Email confirmation is needed.

# Private Part
***

People that are not authenticated cannot see any user-specific details, nor contests or photos.

- **Dashboard** : When the user has successfully logged in, he will be able to see its Dashboard. There are most important tabs for each user. Admin dashboard differs.
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622891119/screenshots/Screenshot_2021-06-05_140508_enqtvc.png)

- **Clicking username** : Every user has profile page with base info for it. It can edit it from Edit Profile button.
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622891196/screenshots/Screenshot_2021-06-05_140625_pq2nlh.png)

- **Listing contests** : Photographers have access to open contests, currently participating contests and contest in which they were participants. Admins can see all contests.
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622891805/screenshots/Screenshot_2021-06-05_141635_h4yu9e.png)

- **Contest page** : Page with info for a contest and additional option by phase. Here jury reviews photos in Phase Two. In Phase Finished are listed winning photos.
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622892229/screenshots/Screenshot_2021-06-05_142242_adz0xw.png)

- **Notifications** : Photographers receive notifications for changes in contests in which they participate. When photographer is invited for jury, it also receive notification. 
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622892448/screenshots/Screenshot_2021-06-05_142506_hbjslw.png)

- **Contest create page** : Access only for Admins. 
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622892513/screenshots/Screenshot_2021-06-05_142824_k7ha5l.png)

- **Photo upload page** : Access only for Photographers which join contest
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622892639/screenshots/Screenshot_2021-06-05_143031_jycv84.png)

- **Photo page** : Information for photo
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1622892977/screenshots/Screenshot_2021-06-05_143610_efysti.png)
