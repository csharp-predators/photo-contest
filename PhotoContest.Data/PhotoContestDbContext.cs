﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoContest.Data
{
    /// <summary>
    /// DB Context for Photo Contest application
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityDbContext{PhotoContest.Data.Models.User, PhotoContest.Data.Models.Role, System.Guid}" />
    public class PhotoContestDbContext : IdentityDbContext<User, Role, Guid>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PhotoContestDbContext"/> class.
        /// </summary>
        /// <param name="options">The options.</param>
        public PhotoContestDbContext(DbContextOptions<PhotoContestDbContext> options)
            : base(options)
        {
        }

        #endregion

        #region DbSets
        /// <summary>
        /// Gets or sets the contests.
        /// </summary>
        public DbSet<Contest> Contests { get; set; }
        /// <summary>
        /// Gets or sets the photos.
        /// </summary>
        public DbSet<Photo> Photos { get; set; }
        /// <summary>
        /// Gets or sets the images.
        /// </summary>
        public DbSet<Image> Images { get; set; }
        /// <summary>
        /// Gets or sets the reviews.
        /// </summary>
        public DbSet<Review> Reviews { get; set; }
        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        public DbSet<Category> Categories { get; set; }
        /// <summary>
        /// Gets or sets the notifications.
        /// </summary>
        public DbSet<Notification> Notifications { get; set; }

        #endregion

        #region Configuration
        /// <summary>
        /// Configures the schema needed for the identity framework.
        /// </summary>
        /// <param name="builder">The builder being used to construct the model for this context.</param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            foreach (Microsoft.EntityFrameworkCore.Metadata.IMutableForeignKey foreignKey in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

            // Configuring many to many relation between Contest and Photographer classes
            builder.Entity<ContestParticipant>()
                .HasKey(x => new { x.ContestId, x.ParticipantId });
            builder.Entity<ContestParticipant>()
                .HasOne(x => x.Contest)
                .WithMany(o => o.Participants);
            builder.Entity<ContestParticipant>()
                .HasOne(x => x.Participant)
                .WithMany(o => o.ContestsAsParticipant);

            // Configuring many to many relation between Contest and Photo classes
            builder.Entity<ContestPhoto>()
                .HasKey(x => new { x.ContestId, x.PhotoId });
            builder.Entity<ContestPhoto>()
                .HasOne(x => x.Contest)
                .WithMany(o => o.Photos);
            builder.Entity<ContestPhoto>()
                .HasOne(x => x.Photo)
                .WithMany(o => o.Contests);

            // Configuring many to many relation between Contest and Users(Jury) classes
            builder.Entity<ContestJury>()
                .HasKey(x => new { x.ContestId, x.JuryId });
            builder.Entity<ContestJury>()
                .HasOne(x => x.Contest)
                .WithMany(o => o.Jury);
            builder.Entity<ContestJury>()
                .HasOne(x => x.Jury)
                .WithMany(o => o.ContestsAsJury);

            // Configuring soft delete for EF Core
            builder.Entity<User>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            builder.Entity<Category>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            builder.Entity<Contest>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            builder.Entity<Image>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            builder.Entity<Photo>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            builder.Entity<Review>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);

            SeedDatabase(builder);
        }

        /// <summary>
        /// <para>
        /// Override this method to configure the database (and other options) to be used for this context.
        /// This method is called for each instance of the context that is created.
        /// The base implementation does nothing.
        /// </para>
        /// <para>
        /// In situations where an instance of <see cref="T:Microsoft.EntityFrameworkCore.DbContextOptions" /> may or may not have been passed
        /// to the constructor, you can use <see cref="P:Microsoft.EntityFrameworkCore.DbContextOptionsBuilder.IsConfigured" /> to determine if
        /// the options have already been set, and skip some or all of the logic in
        /// <see cref="M:Microsoft.EntityFrameworkCore.DbContext.OnConfiguring(Microsoft.EntityFrameworkCore.DbContextOptionsBuilder)" />.
        /// </para>
        /// </summary>
        /// <param name="optionsBuilder">A builder used to create or modify options for this context. Databases (and other extensions)
        /// typically define extension methods on this object that allow you to configure the context.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        /// <summary>
        /// <para>
        /// Saves all changes made in this context to the database.
        /// </para>
        /// <para>
        /// This method will automatically call <see cref="M:Microsoft.EntityFrameworkCore.ChangeTracking.ChangeTracker.DetectChanges" /> to discover any
        /// changes to entity instances before saving to the underlying database. This can be disabled via
        /// <see cref="P:Microsoft.EntityFrameworkCore.ChangeTracking.ChangeTracker.AutoDetectChangesEnabled" />.
        /// </para>
        /// </summary>
        /// <returns>
        /// The number of state entries written to the database.
        /// </returns>
        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        /// <summary>
        /// <para>
        /// Saves all changes made in this context to the database.
        /// </para>
        /// <para>
        /// This method will automatically call <see cref="M:Microsoft.EntityFrameworkCore.ChangeTracking.ChangeTracker.DetectChanges" /> to discover any
        /// changes to entity instances before saving to the underlying database. This can be disabled via
        /// <see cref="P:Microsoft.EntityFrameworkCore.ChangeTracking.ChangeTracker.AutoDetectChangesEnabled" />.
        /// </para>
        /// <para>
        /// Multiple active operations on the same context instance are not supported.  Use 'await' to ensure
        /// that any asynchronous operations have completed before calling another method on this context.
        /// </para>
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">Indicates whether <see cref="M:Microsoft.EntityFrameworkCore.ChangeTracking.ChangeTracker.AcceptAllChanges" /> is called after the changes have
        /// been sent successfully to the database.</param>
        /// <param name="cancellationToken">A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.</param>
        /// <returns>
        /// A task that represents the asynchronous save operation. The task result contains the
        /// number of state entries written to the database.
        /// </returns>
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        /// <summary>
        /// Seeds the database.
        /// </summary>
        /// <param name="builder">The builder.</param>
        private void SeedDatabase(ModelBuilder builder)
        {
            //Password Hasher
            PasswordHasher<User> passHasher = new PasswordHasher<User>();

            Guid avatarId = Guid.NewGuid();
            Image avatar = new Image()
            {
                Id = avatarId,
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1621544431/avatars/default_avatar.png",
                CloudinaryId = "default_avatar"
            };
            builder.Entity<Image>().HasData(avatar);

            //Organisers
            Guid organiserId = Guid.NewGuid();
            User organiser = new User
            {
                Id = organiserId,
                FirstName = "Krastyu",
                LastName = "Terziev",
                UserName = "kyciterziev",
                NormalizedUserName = "KYCITERZIEV",
                Email = "krastyu@terziev.com",
                NormalizedEmail = "KRASTYU@TERZIEV.COM",
                CreatedOn = DateTime.Now,
                IsAdmin = true,
                AvatarId = avatarId
            };
            organiser.PasswordHash = passHasher.HashPassword(organiser, "admin123");
            organiser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(organiser);

            //PhotoJunkies
            Guid photoJunkieId = Guid.NewGuid();
            User photoJunkie = new User
            {
                Id = photoJunkieId,
                FirstName = "Vasil",
                LastName = "Evlogiev",
                UserName = "vasilevglogiev",
                NormalizedUserName = "VASILEVLOGIEV",
                Email = "vasil@evlogiev.com",
                NormalizedEmail = "VASIL@EVLOGIEV.COM",
                CreatedOn = DateTime.Now,
                AvatarId = avatarId
            };
            photoJunkie.PasswordHash = passHasher.HashPassword(photoJunkie, "photojunkie123");
            photoJunkie.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(photoJunkie);

            //Roles
            Guid firstRoleId = Guid.NewGuid();
            Guid secondRoleId = Guid.NewGuid();
            builder.Entity<Role>().HasData(
                new Role { Id = firstRoleId, Name = "Admin", NormalizedName = "ADMIN" },
                new Role { Id = secondRoleId, Name = "Junkie", NormalizedName = "JUNKIE" }
            );

            //Link organiser to Role
            IdentityUserRole<Guid> organiserUserRole = new IdentityUserRole<Guid>
            {
                RoleId = firstRoleId,
                UserId = organiserId
            };
            builder.Entity<IdentityUserRole<Guid>>().HasData(organiserUserRole);

            //Link photoJunkie to Role
            IdentityUserRole<Guid> phtoJunkieUserRole = new IdentityUserRole<Guid>
            {
                RoleId = secondRoleId,
                UserId = photoJunkieId
            };
            builder.Entity<IdentityUserRole<Guid>>().HasData(phtoJunkieUserRole);

            //Categories
            Category category = new Category()
            {
                Id = 1,
                Name = "Nature"
            };
            builder.Entity<Category>().HasData(category);

            //Default Cover Photo for Contest
            Guid coverId = Guid.NewGuid();
            Image coverPhoto = new Image()
            {
                Id = coverId,
                ImageUrl = "https://res.cloudinary.com/predatorsphotocloud/image/upload/v1621787351/covers/CoverPhotoContest_hamcws.jpg",
                CloudinaryId = "CoverPhotoContest_hamcws"
            };
            builder.Entity<Image>().HasData(coverPhoto);

            //Contests
            Guid contestId = Guid.NewGuid();
            Contest contest = new Contest()
            {
                Id = contestId,
                Title = "First Contest",
                CategoryId = 1,
                CreatorId = organiserId,
                CoverId = coverId,
                //JuryId= juryId,
                Phase = "Phase One",
                PhaseOneFinishTime = DateTime.Now.AddDays(1),
                PhaseTwoFinishTime = DateTime.Now.AddDays(2),
            };
            builder.Entity<Contest>().HasData(contest);

            Guid secondContestId = Guid.NewGuid();
            Contest secondContest = new Contest()
            {
                Id = secondContestId,
                Title = "Second Contest",
                CategoryId = 1,
                CreatorId = organiserId,
                CoverId = coverId,
                //JuryId= juryId,
                Phase = "Phase One",
                PhaseOneFinishTime = DateTime.Now.AddDays(1),
                PhaseTwoFinishTime = DateTime.Now.AddDays(2),
            };
            builder.Entity<Contest>().HasData(secondContest);

            //Link Contest to User
            ContestParticipant contestUser = new ContestParticipant
            {
                ContestId = contest.Id,
                ParticipantId = photoJunkie.Id
            };
            builder.Entity<ContestParticipant>().HasData(contestUser);

            //Link Contest to User
            ContestParticipant contestUserSecond = new ContestParticipant
            {
                ContestId = secondContest.Id,
                ParticipantId = photoJunkie.Id
            };
            builder.Entity<ContestParticipant>().HasData(contestUserSecond);

            //Link Jury to contest
            ContestJury contestJury = new ContestJury
            {
                ContestId = contestId,
                JuryId = organiserId
            };

            ContestJury contestJury2 = new ContestJury
            {
                ContestId = contestId,
                JuryId = photoJunkieId
            };

            builder.Entity<ContestJury>().HasData(contestJury);
            builder.Entity<ContestJury>().HasData(contestJury2);

            //Photos
            Guid photoId = Guid.NewGuid();
            Photo photo = new Photo()
            {
                Id = photoId,
                Title = "First Photo",
                Description = "This is a descripton for the first photo",
                ImageUrl = "somePathToImage",
                CloudinaryId = "somePathToClaoudinary",
                AuthorId = photoJunkieId,
            };
            builder.Entity<Photo>().HasData(photo);

            //Link Contest with Photo
            ContestPhoto contestPhoto = new ContestPhoto
            {
                ContestId = contest.Id,
                PhotoId = photo.Id
            };
            builder.Entity<ContestPhoto>().HasData(contestPhoto);

            //Reviews
            Guid reviewId = Guid.NewGuid();
            Review review = new Review()
            {
                Id = reviewId,
                Score = 6,
                Comment = "That's a good job for your first contest",
                FitsCategory = true,
                AuthorId = photoJunkieId,
                ContestId = contestId,
                PhotoId = photoId,
            };
            builder.Entity<Review>().HasData(review);
        }

        /// <summary>
        /// Updates the soft delete statuses.
        /// </summary>
        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries().Where(x => x.Properties.Any(p => p.Metadata.Name == "IsDeleted")))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        break;
                }
            }
        }

        #endregion
    }
}
