﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// User entity type
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.IdentityUser{System.Guid}" />
    public class User : IdentityUser<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
            ContestsAsParticipant = new HashSet<ContestParticipant>();
            ContestsAsJury = new HashSet<ContestJury>();
        }

        /// <summary>
        /// Gets or sets the user name for this user.
        /// </summary>
        [Required]
        [MinLength(2), MaxLength(20)]
        public override string UserName { get => base.UserName; set => base.UserName = value; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [Required]
        [MinLength(2), MaxLength(20)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [Required]
        [MinLength(2), MaxLength(20)]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the avatar identifier.
        /// </summary>
        public Guid AvatarId { get; set; }
        /// <summary>
        /// Gets or sets the avatar.
        /// </summary>
        public Image Avatar { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is admin.
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Gets or sets the points.
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public string Rank { get; set; }

        /// <summary>
        /// Gets or sets the contests as participant.
        /// </summary>
        public HashSet<ContestParticipant> ContestsAsParticipant { get; set; }

        /// <summary>
        /// Gets or sets the contests as jury.
        /// </summary>
        public HashSet<ContestJury> ContestsAsJury { get; set; }
    }
}
