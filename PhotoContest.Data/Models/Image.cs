﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Image entity type
    /// </summary>
    public class Image
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        [Required]
        [Url]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets the cloudinary identifier.
        /// </summary>
        [Required]
        public string CloudinaryId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}