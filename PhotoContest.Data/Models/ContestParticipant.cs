﻿using System;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Many to many relation between Contest and User[Participant]
    /// </summary>
    public class ContestParticipant
    {
        /// <summary>
        /// Gets or sets the contest identifier.
        /// </summary>
        public Guid ContestId { get; set; }
        /// <summary>
        /// Gets or sets the contest.
        /// </summary>
        public Contest Contest { get; set; }

        /// <summary>
        /// Gets or sets the participant identifier.
        /// </summary>
        public Guid ParticipantId { get; set; }
        /// <summary>
        /// Gets or sets the participant.
        /// </summary>
        public User Participant { get; set; }
    }
}