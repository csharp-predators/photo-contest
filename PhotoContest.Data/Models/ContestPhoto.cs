﻿using System;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Many to many relation between Contest and Photo
    /// </summary>
    public class ContestPhoto
    {
        /// <summary>
        /// Gets or sets the contest identifier.
        /// </summary>
        public Guid ContestId { get; set; }
        /// <summary>
        /// Gets or sets the contest.
        /// </summary>
        public Contest Contest { get; set; }

        /// <summary>
        /// Gets or sets the photo identifier.
        /// </summary>
        public Guid PhotoId { get; set; }
        /// <summary>
        /// Gets or sets the photo.
        /// </summary>
        public Photo Photo { get; set; }
    }
}
