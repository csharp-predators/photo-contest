﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Photo entity type
    /// </summary>
    public class Photo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Photo"/> class.
        /// </summary>
        public Photo()
        {
            Reviews = new HashSet<Review>();
            Contests = new HashSet<ContestPhoto>();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [Required]
        [StringLength(20, MinimumLength = 3)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 10)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the author identifier.
        /// </summary>
        public Guid AuthorId { get; set; }
        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        public User Author { get; set; }

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        [Required]
        [Url]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets the cloudinary identifier.
        /// </summary>
        [Required]
        public string CloudinaryId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the reviews.
        /// </summary>
        public HashSet<Review> Reviews { get; set; }

        /// <summary>
        /// Gets or sets the contests.
        /// </summary>
        public HashSet<ContestPhoto> Contests { get; set; }
    }
}
