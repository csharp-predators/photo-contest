﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Notification entity type
    /// </summary>
    public class Notification
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Gets or sets the contest identifier.
        /// </summary>
        public Guid ContestId { get; set; }
        /// <summary>
        /// Gets or sets the contest.
        /// </summary>
        public Contest Contest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is readed.
        /// </summary>
        public bool IsReaded { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }
    }
}
