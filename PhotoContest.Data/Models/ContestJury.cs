﻿using System;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Many to many relation between Contest and User[Jury]
    /// </summary>
    public class ContestJury
    {
        /// <summary>
        /// Gets or sets the jury identifier.
        /// </summary>
        public Guid JuryId { get; set; }
        /// <summary>
        /// Gets or sets the jury.
        /// </summary>
        public User Jury { get; set; }

        /// <summary>
        /// Gets or sets the contest identifier.
        /// </summary>
        public Guid ContestId { get; set; }
        /// <summary>
        /// Gets or sets the contest.
        /// </summary>
        public Contest Contest { get; set; }
    }
}
