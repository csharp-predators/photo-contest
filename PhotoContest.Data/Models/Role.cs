﻿using Microsoft.AspNetCore.Identity;
using System;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Role entity type for User roles
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.IdentityRole{System.Guid}" />
    public class Role : IdentityRole<Guid>
    {

    }
}
