﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Contest entity type
    /// </summary>
    public class Contest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Contest"/> class.
        /// </summary>
        public Contest()
        {
            Jury = new HashSet<ContestJury>();
            Participants = new HashSet<ContestParticipant>();
            Photos = new HashSet<ContestPhoto>();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is invitational.
        /// </summary>
        public bool IsInvitational { get; set; }

        /// <summary>
        /// Gets or sets the category identifier.
        /// </summary>
        public int CategoryId { get; set; }
        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// Gets or sets the creator identifier.
        /// </summary>
        public Guid CreatorId { get; set; }
        /// <summary>
        /// Gets or sets the creator.
        /// </summary>
        public User Creator { get; set; }

        /// <summary>
        /// Gets or sets the phase.
        /// </summary>
        public string Phase { get; set; }

        /// <summary>
        /// Gets or sets the cover identifier.
        /// </summary>
        public Guid CoverId { get; set; }
        /// <summary>
        /// Gets or sets the cover.
        /// </summary>
        public Image Cover { get; set; }

        /// <summary>
        /// Gets or sets the phase one finish time.
        /// </summary>

        public DateTime PhaseOneFinishTime { get; set; }

        /// <summary>
        /// Gets or sets the phase two finish time.
        /// </summary>
        public DateTime PhaseTwoFinishTime { get; set; }

        /// <summary>
        /// Gets or sets the jury.
        /// </summary>
        public HashSet<ContestJury> Jury { get; set; }

        /// <summary>
        /// Gets or sets the participants.
        /// </summary>
        public HashSet<ContestParticipant> Participants { get; set; }

        /// <summary>
        /// Gets or sets the photos.
        /// </summary>
        public HashSet<ContestPhoto> Photos { get; set; }
    }
}