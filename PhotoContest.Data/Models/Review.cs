﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Data.Models
{
    /// <summary>
    /// Review entity type
    /// </summary>
    public class Review
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        [Required]
        [Range(0, 10)]
        public int Score { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 10)]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [fits category].
        /// </summary>
        public bool FitsCategory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the author identifier.
        /// </summary>
        public Guid AuthorId { get; set; }
        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        public User Author { get; set; }

        /// <summary>
        /// Gets or sets the contest identifier.
        /// </summary>
        public Guid ContestId { get; set; }
        /// <summary>
        /// Gets or sets the contest.
        /// </summary>
        public Contest Contest { get; set; }

        /// <summary>
        /// Gets or sets the photo identifier.
        /// </summary>
        public Guid PhotoId { get; set; }
        /// <summary>
        /// Gets or sets the photo.
        /// </summary>
        public Photo Photo { get; set; }
    }
}
