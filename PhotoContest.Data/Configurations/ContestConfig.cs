﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Data.Models;

namespace PhotoContest.Data.Configurations
{
    public class ContestConfig : IEntityTypeConfiguration<Contest>
    {
        /// <summary>Configures the entity of type Contest</summary>
        /// <param name="builder">The builder to be used to configure the entity type Contest.</param>
        public void Configure(EntityTypeBuilder<Contest> builder)
        {
            builder.HasIndex(x => x.Title).IsUnique();
        }
    }
}
