﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Data.Models;

namespace PhotoContest.Data.Configurations
{
    public class CategoryConfig : IEntityTypeConfiguration<Category>
    {
        /// <summary>Configures the entity of type Category.</summary>
        /// <param name="builder">The builder to be used to configure the entity type Category.</param>
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasIndex(x => x.Name).IsUnique();
        }
    }
}