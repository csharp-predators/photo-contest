﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Data.Models;

namespace PhotoContest.Data.Configurations
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        /// <summary>Configures the entity of type User.</summary>
        /// <param name="builder">The builder to be used to configure the entity type User.</param>
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(x => x.UserName).IsUnique();
            builder.HasIndex(x => x.Email).IsUnique();
        }
    }
}